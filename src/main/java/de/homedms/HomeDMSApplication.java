package de.homedms;

import java.io.File;

import de.homedms.application.DMSApplicationService;
import de.homedms.application.FileRecordRepository;
import de.homedms.application.subservices.PropertyService;
import de.homedms.plugins.SQLiteDatabase;
import de.homedms.plugins.view.control.ApplicationUIControl;
import de.homedms.plugins.view.control.DetailUIControl;
import de.homedms.plugins.view.ui.base.ApplicationView;
import javafx.application.Application;
import javafx.stage.Stage;

public class HomeDMSApplication extends Application {
	
	private ApplicationView applicationView;
	private static String applicationLocationPath;
	
	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		applicationLocationPath = new File(HomeDMSApplication.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile().getParentFile().getPath() + "\\";
		
		FileRecordRepository fileRecordRepository = new FileRecordRepository();
		SQLiteDatabase database = new SQLiteDatabase();
		database.connect();
		
		DMSApplicationService applicationService = new DMSApplicationService(fileRecordRepository, fileRecordRepository, database);
		
		applicationView = new ApplicationView();
		
		ApplicationUIControl applicationUIControl = new ApplicationUIControl(applicationView, applicationService);
		fileRecordRepository.addObserver(applicationUIControl);
		
		DetailUIControl detailUIControl = new DetailUIControl(applicationView, applicationService);
		fileRecordRepository.addObserver(detailUIControl);
		
		applicationService.submitDocumentPath(PropertyService.getProperties().getProperty(PropertyService.PROPERTY_LAST_PATH));
	}
	
	public static String getApplicationLocationPath() {
		
		return applicationLocationPath;
	}
}
