package de.homedms.domain;

import java.io.File;
import java.util.Optional;
import java.util.UUID;

public class FileRecordEntity {
	
	private final UUID uuid;
	
	private final File file;
	private final FileRecordMetaVO fileRecordMetaVO;
	
	private Optional<ADetailRecord> detailRecord;
	private boolean selectedState;
	
	public FileRecordEntity(File file, ADetailRecord detailRecord) {
		
		uuid = UUID.randomUUID();
		
		this.file = file;
		fileRecordMetaVO = new FileRecordMetaVO(
				file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - file.getName().length()),
				file.getName(),
				file.lastModified(),
				file.length());
		
		this.detailRecord = Optional.ofNullable(detailRecord);
		selectedState = false;
	}
	
	public UUID getUUID() {
		
		return uuid;
	}
	
	public File getFile() {
		
		return file;
	}
	
	public FileRecordMetaVO getFileRecordMetaVO() {
		
		return fileRecordMetaVO;
	}
	
	public boolean isSelected() {
		
		return selectedState;
	}
	
	public void setSelectedState(boolean selectedState) {
		
		this.selectedState = selectedState;
	}
	
	public Optional<ADetailRecord> getDetailRecord() {
		
		return detailRecord;
	}
	
	public void setDetailRecord(ADetailRecord detailRecord) {
		
		this.detailRecord = Optional.ofNullable(detailRecord);
	}
	
}
