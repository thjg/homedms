package de.homedms.domain;

import java.util.Objects;

public final class FileRecordMetaVO {
	
	private final String directoryPath;
	private final String name;
	private final long lastModified;
	private final long fileSizeInBytes;
	
	public FileRecordMetaVO(String directoryPath, String name, long lastModified, long fileSizeInBytes) {
		
		this.directoryPath = directoryPath;
		this.name = name;
		this.lastModified = lastModified;
		this.fileSizeInBytes = fileSizeInBytes;
	}
	
	public String getDirectoryPath() {
		
		return directoryPath;
	}
	
	public String getName() {
		
		return name;
	}
	
	public long getLastModified() {
		
		return lastModified;
	}
	
	public long getFileSizeInBytes() {
		
		return fileSizeInBytes;
	}
	
	public String getFileSizeInReadableFormat() {
		
		return convertBytesToReadableFormat(fileSizeInBytes);
	}
	
	private static String convertBytesToReadableFormat(long bytes) {
		
		double dataSize = bytes;
		
		int nRuns = 0;
		while (dataSize / 1000 >= 1 && nRuns < 4) {
			dataSize /= 1000;
			nRuns++;
		}
		
		dataSize = Math.round(dataSize*1000) / 1000.0;
		
		String readableFormat = (dataSize + " ").replace(".", ",");
		
		switch (nRuns) {
		case 1: return readableFormat + "KB";
		case 2: return readableFormat + "MB";
		case 3: return readableFormat + "GB";
		case 4: return readableFormat + "TB";
		default: return readableFormat + "B";
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(directoryPath, fileSizeInBytes, lastModified, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileRecordMetaVO other = (FileRecordMetaVO) obj;
		return Objects.equals(directoryPath, other.directoryPath) && fileSizeInBytes == other.fileSizeInBytes
				&& lastModified == other.lastModified && Objects.equals(name, other.name);
	}

}
