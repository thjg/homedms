package de.homedms.domain;

public enum FileRecordType {
	
	STANDARD("Standard"),
	BILL("Rechnung"),
	ACCOUNT_STATEMENT("Kontoauszug");
	
	private String description;
	
	FileRecordType(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return description;
	}
	
	public static FileRecordType of(String fileType) {
		
		if (fileType == null || fileType.equals("Standard")) {
			return STANDARD;
		} else if (fileType.equals("Rechnung")) {
			return BILL;
		} else if (fileType.equals("Kontoauszug")) {
			return ACCOUNT_STATEMENT;
		} else {
			return STANDARD;
		}
	}
}
