package de.homedms.domain;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IFileRecordRepository {
	
	public List<FileRecordEntity> getFileRecordEntities();
	public List<FileRecordEntity> getFilteredFileRecordEntities();
	
	public FileRecordEntity getFileRecordEntityByUUID(UUID uuid);
	
	public String getDocumentPath();
	public void setDocumentPath(String documentPath);
	
	public String getSearchTerm();
	public void setSearchTerm(String searchTerm);
	
	public FileRecordType getSelectionFileRecordType();
	public void setSelectionFileRecordType(FileRecordType selectionFileRecordType);
	
	public Optional<ADetailRecord> getDetailRecordForEditing();
	public void setDetailRecordForEditing(Optional<ADetailRecord> detailRecordForEditing);
}
