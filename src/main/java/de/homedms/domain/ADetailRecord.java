package de.homedms.domain;

import java.util.Objects;

public abstract class ADetailRecord {
	
	protected String directoryPath;
	protected String name;
	protected FileRecordType fileRecordType;
	
	public ADetailRecord(String directoryPath, String name, FileRecordType fileRecordType) {
		
		this.directoryPath = directoryPath;
		this.name = name;
		this.fileRecordType = fileRecordType;
	}
	
	public String getDirectoryPath() {
		
		return directoryPath;
	}
	
	public String getName() {
		
		return name;
	}
	
	public FileRecordType getFileRecordType() {
		
		return fileRecordType;
	}
	
	public abstract ADetailRecord copy();
	public abstract boolean isDefault();

	@Override
	public int hashCode() {
		return Objects.hash(directoryPath, fileRecordType, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADetailRecord other = (ADetailRecord) obj;
		return Objects.equals(directoryPath, other.directoryPath) && fileRecordType == other.fileRecordType
				&& Objects.equals(name, other.name);
	}
}