package de.homedms.domain.detailrecords;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

import de.homedms.domain.ADetailRecord;
import de.homedms.domain.FileRecordType;

public final class BillDetailRecordVO extends ADetailRecord {
	
	private final Optional<String> notes;
	private final Optional<LocalDate> billDate;
	private final Optional<LocalDate> payDate;
	
	public BillDetailRecordVO(String directoryPath, String name, Optional<String> notes, Optional<LocalDate> billDate, Optional<LocalDate> payDate) {
		super(directoryPath, name, FileRecordType.BILL);
		
		if (notes.isPresent() && notes.get().equals("")) {
			notes = Optional.empty();
		}
		this.notes = notes;
		this.billDate = billDate;
		this.payDate = payDate;
	}
	
	public Optional<String> getNotes() {
		
		return notes;
	}
	
	public Optional<LocalDate> getBillDate() {
		
		return billDate;
	}
	
	public Optional<LocalDate> getPayDate() {
		
		return payDate;
	}
	
	@Override
	public BillDetailRecordVO copy() {
		
		return new BillDetailRecordVO(directoryPath, name, notes, billDate, payDate);
	}
	
	@Override
	public boolean isDefault() {
		
		return this.equals(BillDetailRecordVO.createDefault(directoryPath, name));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(billDate, notes, payDate);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillDetailRecordVO other = (BillDetailRecordVO) obj;
		return Objects.equals(billDate, other.billDate) && Objects.equals(notes, other.notes)
				&& Objects.equals(payDate, other.payDate);
	}

	public static BillDetailRecordVO createDefault(String directoryPath, String name) {
		
		return new BillDetailRecordVO(directoryPath, name, Optional.empty(), Optional.empty(), Optional.empty());
	}
}
