package de.homedms.domain.detailrecords;

import java.util.Objects;
import java.util.Optional;

import de.homedms.domain.ADetailRecord;
import de.homedms.domain.FileRecordType;

public final class StandardDetailRecordVO extends ADetailRecord {
	
	private final Optional<String> notes;
	
	public StandardDetailRecordVO(String directoryPath, String name, Optional<String> notes) {
		super(directoryPath, name, FileRecordType.STANDARD);
		
		if (notes.isPresent() && notes.get().equals("")) {
			notes = Optional.empty();
		}
		this.notes = notes;
	}
	
	
	public Optional<String> getNotes() {
		
		return notes;
	}

	@Override
	public StandardDetailRecordVO copy() {
		
		return new StandardDetailRecordVO(directoryPath, name, notes);
	}
	
	@Override
	public boolean isDefault() {
		
		return this.equals(StandardDetailRecordVO.createDefault(directoryPath, name));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(notes);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		StandardDetailRecordVO other = (StandardDetailRecordVO) obj;
		return Objects.equals(notes, other.notes);
	}


	public static StandardDetailRecordVO createDefault(String directoryPath, String name) {
		
		return new StandardDetailRecordVO(directoryPath, name, Optional.empty());
	}
}
