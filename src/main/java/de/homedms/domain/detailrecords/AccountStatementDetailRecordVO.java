package de.homedms.domain.detailrecords;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

import de.homedms.domain.ADetailRecord;
import de.homedms.domain.FileRecordType;

public final class AccountStatementDetailRecordVO extends ADetailRecord {
	
	private final Optional<String> notes;
	private final Optional<LocalDate> dateOfIssue;
	private final Optional<String> bank;
	
	public AccountStatementDetailRecordVO(String directoryPath, String name, Optional<String> notes, Optional<LocalDate> dateOfIssue, Optional<String> bank) {
		super(directoryPath, name, FileRecordType.ACCOUNT_STATEMENT);
		
		if (notes.isPresent() && notes.get().equals("")) {
			notes = Optional.empty();
		}
		this.notes = notes;
		this.dateOfIssue = dateOfIssue;
		this.bank = bank;
	}
	
	public Optional<String> getNotes() {
		
		return notes;
	}
	
	public Optional<LocalDate> getDateOfIssue() {
		
		return dateOfIssue;
	}
	
	public Optional<String> getBank() {
		
		return bank;
	}
	
	@Override
	public AccountStatementDetailRecordVO copy() {
		
		return new AccountStatementDetailRecordVO(directoryPath, name, notes, dateOfIssue, bank);
	}
	
	@Override
	public boolean isDefault() {
		
		return this.equals(AccountStatementDetailRecordVO.createDefault(directoryPath, name));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(bank, dateOfIssue, notes);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountStatementDetailRecordVO other = (AccountStatementDetailRecordVO) obj;
		return Objects.equals(bank, other.bank) && Objects.equals(dateOfIssue, other.dateOfIssue)
				&& Objects.equals(notes, other.notes);
	}

	public static AccountStatementDetailRecordVO createDefault(String directoryPath, String name) {
		
		return new AccountStatementDetailRecordVO(directoryPath, name, Optional.empty(), Optional.empty(), Optional.empty());
	}
}
