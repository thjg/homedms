package de.homedms.plugins;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.homedms.HomeDMSApplication;
import de.homedms.application.util.IDatabase;

public class SQLiteDatabase implements IDatabase {
	
	private static final String DATABASE_PATH = "jdbc:sqlite:" + HomeDMSApplication.getApplicationLocationPath();
	private static final String DB_MAIN = "DataHomeDMS.db";
	
	private Connection connection;
	
	public void connect() {
		
		try {
			connection = DriverManager.getConnection(DATABASE_PATH + DB_MAIN);
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}
	
	private Connection getConnection() throws SQLException {
		
		if (connection == null || connection.isClosed()) {
			connect();
		}
		return connection;
	}
	
	@Override
	public ResultSet executeSelect(String sql, String... params) throws SQLException {
		
		PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
		for (int i = 1; i < params.length + 1; i++) {
			preparedStatement.setString(i, params[i-1]);
		}
		return preparedStatement.executeQuery();
	}

	@Override
	public void executeInsertOrUpdateOrDelete(String sql, String... params) throws SQLException {
		
		PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
		for (int i = 1; i < params.length + 1; i++) {
			preparedStatement.setString(i, params[i-1]);
		}
		preparedStatement.executeUpdate();
	}

	@Override
	public void executeCreateTable(String sql) throws SQLException {
		
		getConnection().prepareStatement(sql).execute();
	}

}
