package de.homedms.plugins.view.ui.detailarea;

import static de.homedms.application.util.Language.*;

import de.homedms.application.util.Language;
import de.homedms.domain.FileRecordType;
import de.homedms.plugins.view.ui.detailarea.detailfields.AccountStatementDetailFieldsArea;
import de.homedms.plugins.view.ui.detailarea.detailfields.BillDetailFieldsArea;
import de.homedms.plugins.view.ui.detailarea.detailfields.DetailFieldsArea;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class DetailArea extends VBox {
	
	//Accessible Components
	
	private TextField filenameTextField;
	private Button openFileButton;
	private DetailFieldsArea detailFieldsArea;
	private FootArea footArea;
	
	//Layout Components
	
	private HBox interactionFileButtonBox;
	
	
	public DetailArea(FileRecordType fileRecordType) {
		
		initDetailAreaComponents();
		addDetailAreaComponents();
		initDetailFieldsArea(fileRecordType);
		addDetailFieldsArea();
		initFootArea();
		addFootArea();
		
		this.setPadding(new Insets(5));
	}
	
	
	public DetailFieldsArea getDetailFieldsArea() {
		
		return detailFieldsArea;
	}
	
	public TextField getFilenameTextField() {
		
		return filenameTextField;
	}
	
	public Button getOpenFileButton() {
		
		return openFileButton;
	}
	
	public FootArea getFootArea() {
		
		return footArea;
	}
	
	private void initDetailAreaComponents() {
		
		filenameTextField = new TextField("Dummy.pdf");
		filenameTextField.setEditable(false);
		filenameTextField.setBackground(null);
		filenameTextField.setAlignment(Pos.CENTER);
		filenameTextField.setFont(new Font(24));
		
		interactionFileButtonBox = new HBox();
		interactionFileButtonBox.setAlignment(Pos.CENTER);
		interactionFileButtonBox.setPadding(new Insets(5));
		
		openFileButton = new Button(Language.get(COMPONENT_DETAILAREA_OPENFILEBUTTONDESCRIPTION));
		openFileButton.setId("openFileButton");
		interactionFileButtonBox.getChildren().add(openFileButton);
	}
	
	private void addDetailAreaComponents() {
		
		this.getChildren().add(filenameTextField);
		this.getChildren().add(interactionFileButtonBox);
	}
	
	@SuppressWarnings("static-access")
	private void initDetailFieldsArea(FileRecordType fileRecordType) {
		
		switch (fileRecordType) {
		case STANDARD: {
			detailFieldsArea = new DetailFieldsArea();
			break;
		}
		case BILL: {
			detailFieldsArea = new BillDetailFieldsArea();
			break;
		}
		case ACCOUNT_STATEMENT: {
			detailFieldsArea = new AccountStatementDetailFieldsArea();
			break;
		}
		}
		this.setVgrow(detailFieldsArea, Priority.ALWAYS);
	}
	
	private void addDetailFieldsArea() {
		
		this.getChildren().add(detailFieldsArea);
	}
	
	private void initFootArea() {
		
		footArea = new FootArea();
	}
	
	private void addFootArea() {
		
		this.getChildren().add(footArea);
	}
	
}
