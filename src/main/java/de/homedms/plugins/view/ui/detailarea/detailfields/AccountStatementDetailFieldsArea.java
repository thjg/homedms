package de.homedms.plugins.view.ui.detailarea.detailfields;

import static de.homedms.application.util.Language.*;

import de.homedms.application.util.Language;
import de.homedms.domain.FileRecordType;
import de.homedms.plugins.view.ui.components.DataFieldsDatePicker;
import javafx.geometry.Insets;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class AccountStatementDetailFieldsArea extends DetailFieldsArea {
	
	//Accessible Components
	
	private Label accountStatementDateLabel;
	private DataFieldsDatePicker accountStatementDatePickerComponent;
	
	private Label bankLabel;
	private TextField bankTextField;
	
	
	public AccountStatementDetailFieldsArea() {
		
		initAccountStatementDetailFieldsComponents();
		addAccountStatementDetailFieldsToContentGridRight();
	}
	
	
	public DatePicker getAccountStatementDatePicker() {
		
		return accountStatementDatePickerComponent;
	}
	
	public TextField getBankTextField() {
		
		return bankTextField;
	}
	
	private void initAccountStatementDetailFieldsComponents() {
		
		accountStatementDateLabel = new Label(Language.get(COMPONENT_DETAILAREA_LABEL_ACCOUNTSTATEMENT_CREATEDATE));
		accountStatementDateLabel.setPadding(new Insets(3));
		accountStatementDatePickerComponent = new DataFieldsDatePicker();
		
		bankLabel = new Label(Language.get(COMPONENT_DETAILAREA_LABEL_ACCOUNTSTATEMENT_BANK));
		bankLabel.setPadding(new Insets(3));
		bankTextField = new TextField();
		
		dataTypeComboBox.setValue(FileRecordType.ACCOUNT_STATEMENT.toString());
	}
	
	private void addAccountStatementDetailFieldsToContentGridRight() {
		
		detailFieldsContentGridRight.add(accountStatementDateLabel, 0, 1);
		detailFieldsContentGridRight.add(accountStatementDatePickerComponent, 1, 1);
		
		detailFieldsContentGridRight.add(bankLabel, 0, 2);
		detailFieldsContentGridRight.add(bankTextField, 1, 2);
	}
	
}
