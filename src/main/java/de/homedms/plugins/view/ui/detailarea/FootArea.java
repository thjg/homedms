package de.homedms.plugins.view.ui.detailarea;

import static de.homedms.application.util.Language.*;

import de.homedms.application.util.Language;
import de.homedms.plugins.view.util.ColumnConstraintsBuilder;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class FootArea extends GridPane {
	
	//Accessible Components
	
	private Button saveButton;
	private Button resetButton;
	private Button deleteButton;
	
	//Layout Components
	
	private HBox buttonBox;
	
	
	public FootArea() {
		
		initFootGrid();
		initFootComponents();
		addFootComponents();
	}
	
	public Button getSaveButton() {
		
		return saveButton;
	}
	
	public Button getResetButton() {
		
		return resetButton;
	}
	
	public Button getDeleteButton() {
		
		return deleteButton;
	}
	
	
	private void initFootGrid() {

		this.getColumnConstraints().add(new ColumnConstraintsBuilder().hAlignmentCenter().percentWidth(100).build());
		
		this.setPadding(new Insets(5));
	}
	
	private void initFootComponents() {
		
		saveButton = new Button(Language.get(COMPONENT_DETAILAREA_SAVECHANGESBUTTONDESCRIPTION));
		resetButton = new Button(Language.get(COMPONENT_DETAILAREA_UNDOCHANGESBUTTONDESCRIPTION));
		deleteButton = new Button(Language.get(COMPONENT_DETAILAREA_DELETEDATABUTTONDESCRIPTION));
		
		buttonBox = new HBox(3);
		buttonBox.setAlignment(Pos.CENTER);
		buttonBox.getChildren().addAll(saveButton, resetButton, deleteButton);
	}
	
	private void addFootComponents() {
		
		this.addColumn(0, buttonBox);
	}
	
}
