package de.homedms.plugins.view.ui.components;

import javafx.scene.control.TextField;
import de.homedms.plugins.view.util.AlertBuilder;
import javafx.scene.control.Alert.AlertType;

public class ErrorableTextField extends TextField {
	
	private static final String C_ERROR_DIRECTORY_INPUT = "errorDirectoryInput";
	
	public ErrorableTextField() {
		super();
	}
	
	public ErrorableTextField(String text) {
		super(text);
	}
	
	public void setError(String errorMessage) {
		
		if (!this.getStyleClass().contains(C_ERROR_DIRECTORY_INPUT)) {
			this.getStyleClass().add(C_ERROR_DIRECTORY_INPUT);
		}
		
		new AlertBuilder(AlertType.ERROR).setText(errorMessage).show();
	}
	
	public boolean isError() {
		
		return this.getStyleClass().contains(C_ERROR_DIRECTORY_INPUT);
	}
	
	public void clearError() {
		
		this.getStyleClass().remove(C_ERROR_DIRECTORY_INPUT);
	}
	
}
