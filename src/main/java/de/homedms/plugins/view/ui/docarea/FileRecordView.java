package de.homedms.plugins.view.ui.docarea;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.UUID;

import javax.swing.ImageIcon;
import javax.swing.filechooser.FileSystemView;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class FileRecordView extends HBox {
	
	private ImageView typeIcon;
	private Label filename;
	private File file;
	private UUID uuid;
	
	public FileRecordView(File file, UUID uuid) {
		
		this.file = file;
		this.uuid = uuid;
		createLayout();
	}
	
	public UUID getUUID() {
		
		return uuid;
	}
	
	public boolean isSelected() {
		
		return this.getStyleClass().contains("fileRecordViewSelected");
	}
	
	public void setSelected(boolean newSelection) {
		
		if (newSelection) {
			if (!isSelected()) {
				this.getStyleClass().add("fileRecordViewSelected");
			}
		} else {
			this.getStyleClass().remove("fileRecordViewSelected");
		}
	}
	
	private void createLayout() {
		
		getStyleClass().add("fileRecordView");
		
		java.awt.Image image = ((ImageIcon) FileSystemView.getFileSystemView().getSystemIcon(file)).getImage();
		BufferedImage systemIconImage = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		Graphics2D imageGraphics = systemIconImage.createGraphics();
		imageGraphics.drawImage(image, 0, 0, null);
		imageGraphics.dispose();
		
		typeIcon = new ImageView(SwingFXUtils.toFXImage(systemIconImage, null));
		typeIcon.setPreserveRatio(true);
		filename = new Label(file.getName());
		if (filename.getText().equals("")) {
			filename.setText(file.getPath());
		}
		filename.getStyleClass().add("labelfile");
		typeIcon.fitHeightProperty().bind(filename.heightProperty());
		this.setPadding(new Insets(2));
		
		this.getChildren().addAll(typeIcon, filename);
	}
}
