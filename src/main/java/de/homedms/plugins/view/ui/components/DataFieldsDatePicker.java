package de.homedms.plugins.view.ui.components;

import javafx.scene.control.DatePicker;
import javafx.scene.input.KeyCode;

public class DataFieldsDatePicker extends DatePicker {
	
	public DataFieldsDatePicker() {
		super();
		
		this.setEditable(false);
		initDeleteAndBackSpaceListener();
	}
	
	private void initDeleteAndBackSpaceListener() {
		
		this.getEditor().setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.DELETE || event.getCode() == KeyCode.BACK_SPACE) {
				this.setValue(null);
			}
		});
	}
}
