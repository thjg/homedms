package de.homedms.plugins.view.ui.detailarea.detailfields;

import static de.homedms.application.util.Language.*;

import de.homedms.application.util.Language;
import de.homedms.domain.FileRecordType;
import de.homedms.plugins.view.util.ColumnConstraintsBuilder;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class DetailFieldsArea extends ScrollPane {
	
	//Accessible Components
	
	private Label changeDateLabel;
	private TextField changeDateTextField;
	
	private Label dataSizeLabel;
	private TextField dataSizeTextField;
	
	private Label dataTypeLabel;
	protected ComboBox<String> dataTypeComboBox;
	
	private Label notesLabel;
	private TextArea notesTextArea;
	
	//Layout Components
	
	private GridPane detailFieldsContentGrid;
	private GridPane detailFieldsContentGridLeft;
	protected GridPane detailFieldsContentGridRight;
	
	
	public DetailFieldsArea() {
		
		initDetailFieldsLayout();
		initDetailFieldsComponents();
		addDetailFieldsToContentGrid();
	}
	
	public TextField getChangeDateTextField() {
		
		return changeDateTextField;
	}
	
	public TextField getDataSizeTextField() {
		
		return dataSizeTextField;
	}
	
	public ComboBox<String> getDataTypeComboBox() {
		
		return dataTypeComboBox;
	}
	
	public TextArea getNotesTextArea() {
		
		return notesTextArea;
	}
	
	@Override
	public void requestFocus() {
		
		//catch it
	}
	
	
	private void initDetailFieldsLayout() {
		
		detailFieldsContentGrid = new GridPane();
		
		detailFieldsContentGrid.getColumnConstraints().add(new ColumnConstraintsBuilder().hAlignmentRight().hGrowAlways().percentWidth(30).build());
		detailFieldsContentGrid.getColumnConstraints().add(new ColumnConstraintsBuilder().hAlignmentLeft().hGrowAlways().percentWidth(70).build());
		
		detailFieldsContentGrid.setPadding(new Insets(5));
		
		detailFieldsContentGridLeft = new GridPane();
		detailFieldsContentGridRight = new GridPane();
		
		detailFieldsContentGridLeft.getColumnConstraints().add(new ColumnConstraintsBuilder().hAlignmentRight().hGrowAlways().percentWidth(50).build());
		detailFieldsContentGridRight.getColumnConstraints().add(new ColumnConstraintsBuilder().hAlignmentRight().hGrowAlways().percentWidth(50).build());
		detailFieldsContentGridLeft.getColumnConstraints().add(new ColumnConstraintsBuilder().hAlignmentLeft().hGrowAlways().percentWidth(70).build());
		detailFieldsContentGridRight.getColumnConstraints().add(new ColumnConstraintsBuilder().hAlignmentLeft().hGrowAlways().percentWidth(70).build());
		
		this.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		this.setFitToWidth(true);
		this.setContent(detailFieldsContentGrid);
	}
	
	
	private void initDetailFieldsComponents() {
		
		changeDateLabel = new Label(Language.get(COMPONENT_DETAILAREA_LABEL_CHANGEDATE));
		changeDateLabel.setPadding(new Insets(3));
		changeDateTextField = new TextField();
		changeDateTextField.setEditable(false);
		changeDateTextField.setText("29.02.2021");
		
		dataSizeLabel = new Label(Language.get(COMPONENT_DETAILAREA_LABEL_FILESIZE));
		dataSizeLabel.setPadding(new Insets(3));
		dataSizeTextField = new TextField();
		dataSizeTextField.setEditable(false);
		dataSizeTextField.setText("527.96 MB");
		
		dataTypeLabel = new Label(Language.get(COMPONENT_DETAILAREA_LABEL_FILETYPE));
		dataTypeLabel.setPadding(new Insets(3));
		dataTypeComboBox = new ComboBox<>();
		dataTypeComboBox.getItems().addAll(FileRecordType.STANDARD.toString(), FileRecordType.BILL.toString(), FileRecordType.ACCOUNT_STATEMENT.toString());
		dataTypeComboBox.setValue(FileRecordType.STANDARD.toString());
		
		notesLabel = new Label(Language.get(COMPONENT_DETAILAREA_LABEL_NOTES));
		notesLabel.setPadding(new Insets(3));
		notesTextArea = new TextArea();
	}
	
	private void addDetailFieldsToContentGrid() {
		
		detailFieldsContentGrid.addColumn(0, detailFieldsContentGridLeft);
		detailFieldsContentGrid.addColumn(1, detailFieldsContentGridRight);
		
		detailFieldsContentGridLeft.addColumn(0, changeDateLabel, dataSizeLabel, dataTypeLabel);
		detailFieldsContentGridLeft.addColumn(1, changeDateTextField, dataSizeTextField, dataTypeComboBox);
		detailFieldsContentGridRight.addColumn(0, notesLabel);
		detailFieldsContentGridRight.addColumn(1, notesTextArea);
	}
	
}
