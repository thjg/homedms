package de.homedms.plugins.view.ui.detailarea.detailfields;

import static de.homedms.application.util.Language.*;

import de.homedms.application.util.Language;
import de.homedms.domain.FileRecordType;
import de.homedms.plugins.view.ui.components.DataFieldsDatePicker;
import javafx.geometry.Insets;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;

public class BillDetailFieldsArea extends DetailFieldsArea {
	
	//Accessible Components
	
	private Label billDateLabel;
	private DataFieldsDatePicker billDatePickerComponent;
	
	private Label payDateLabel;
	private DataFieldsDatePicker payDatePickerComponent;
	
	
	public BillDetailFieldsArea() {
		
		initBillDetailFieldsComponents();
		addBillDetailFieldsToContentGridRight();
	}
	
	
	public DatePicker getBillDatePicker() {
		
		return billDatePickerComponent;
	}
	
	public DatePicker getPayDatePicker() {
		
		return payDatePickerComponent;
	}
	
	private void initBillDetailFieldsComponents() {
		
		billDateLabel = new Label(Language.get(COMPONENT_DETAILAREA_LABEL_BILL_BILLDATE));
		billDateLabel.setPadding(new Insets(3));
		billDatePickerComponent = new DataFieldsDatePicker();
		
		payDateLabel = new Label(Language.get(COMPONENT_DETAILAREA_LABEL_BILL_PAYDATE));
		payDateLabel.setPadding(new Insets(3));
		payDatePickerComponent = new DataFieldsDatePicker();
		
		dataTypeComboBox.setValue(FileRecordType.BILL.toString());
	}
	
	private void addBillDetailFieldsToContentGridRight() {
		
		detailFieldsContentGridRight.add(billDateLabel, 0, 1);
		detailFieldsContentGridRight.add(billDatePickerComponent, 1, 1);
		
		detailFieldsContentGridRight.add(payDateLabel, 0, 2);
		detailFieldsContentGridRight.add(payDatePickerComponent, 1, 2);
	}
}
