package de.homedms.plugins.view.ui.docarea;

import static de.homedms.application.util.Language.*;

import de.homedms.application.util.Language;
import de.homedms.domain.FileRecordType;
import de.homedms.plugins.view.ui.components.ErrorableTextField;
import de.homedms.plugins.view.util.ColumnConstraintsBuilder;
import javafx.geometry.Insets;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class DocumentArea extends VBox {
	
	//Accessible Components
	
	private ErrorableTextField searchTextField;
	private MenuButton selectionButton;
	private DocumentRecordArea documentRecordArea;
	
	//Layout Components
	
	private GridPane searchAreaGrid;
	
	
	public DocumentArea() {
		
		initSearchArea();
		addSearchArea();
		initDocumentRecordArea();
		addDocumentRecordArea();
		
		this.setId("documentArea");
	}
	
	
	public ErrorableTextField getSearchTextField() {
		
		return searchTextField;
	}
	
	public MenuButton getSelectionButton() {
		
		return selectionButton;
	}
	
	public DocumentRecordArea getDocumentRecordArea() {
		
		return documentRecordArea;
	}
	
	private void initSearchArea() {
		
		initSearchAreaGrid();
		initSearchAreaComponents();
		addComponentsToSearchAreaGrid();
	}
	
	private void addSearchArea() {
		
		this.getChildren().add(searchAreaGrid);
	}
	
	private void initDocumentRecordArea() {
		
		documentRecordArea = new DocumentRecordArea();
	}
	
	private void addDocumentRecordArea() {
		
		this.getChildren().add(documentRecordArea);
	}
	
	private void initSearchAreaGrid() {
		
		searchAreaGrid = new GridPane();
		
		searchAreaGrid.getColumnConstraints().add(new ColumnConstraintsBuilder().hGrowAlways().build());
		searchAreaGrid.getColumnConstraints().add(new ColumnConstraintsBuilder().hGrowAlways().build());
		searchAreaGrid.getColumnConstraints().add(new ColumnConstraintsBuilder().hAlignmentRight().hGrowAlways().build());
		
		searchAreaGrid.setPadding(new Insets(10));
	}
	
	private void initSearchAreaComponents() {
		
		searchTextField = new ErrorableTextField();
		searchTextField.setPromptText(Language.get(COMPONENT_DOCUMENTAREA_SEARCHFIELD_PROMPTTEXT));
		
		ImageView imageSelectionButton = new ImageView(getClass().getResource("/de/homedms/images/filter-icon.png").toString());
		imageSelectionButton.fitHeightProperty().bind(searchTextField.heightProperty());
		imageSelectionButton.setPreserveRatio(true);
		
		selectionButton = new MenuButton();
		selectionButton.getItems().addAll(new MenuItem("-"), new MenuItem(FileRecordType.STANDARD.toString()), new MenuItem(FileRecordType.BILL.toString()), new MenuItem(FileRecordType.ACCOUNT_STATEMENT.toString()));
		selectionButton.setGraphic(imageSelectionButton);
		selectionButton.setStyle("-fx-background-color: transparent;");
	}
	
	private void addComponentsToSearchAreaGrid() {
		
		searchAreaGrid.addColumn(0, searchTextField);
		searchAreaGrid.addColumn(2, selectionButton);
	}
	
}