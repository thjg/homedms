package de.homedms.plugins.view.ui.docarea;

import static de.homedms.application.util.Language.*;

import de.homedms.application.util.Language;
import de.homedms.plugins.view.ui.components.ErrorableTextField;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class DocumentRecordArea extends VBox {
	
	//Accessible Components
	
	private Button goBackButton;
	private ErrorableTextField documentPathTextField;
	private Button editDocumentPathButton;
	private Button reloadButton;
	
	private ScrollPane recordScrollPane;
	
	//Layout Components
	
	private HBox headlineBox;
	private VBox recordScrollPaneContentBox;
	
	
	public DocumentRecordArea() {
		
		
		initHeadline();
		initHeadlineComponents();
		addComponentsToHeadlineGrid();
		adjustHeadlineLayout();
		addHeadline();
		
		initRecordScrollPane();
		addRecordScrollPane();
		
		this.setPadding(new Insets(10));
	}
	
	
	public Button getGoBackButton() {
		
		return goBackButton;
	}
	
	public ErrorableTextField getDocumentPathTextField() {
		
		return documentPathTextField;
	}
	
	public Button getEditDocumentPathButton() {
		
		return editDocumentPathButton;
	}
	
	public Button getReloadButton() {
		
		return reloadButton;
	}
	
	public ObservableList<Node> getFileRecordViews() {
		
		return recordScrollPaneContentBox.getChildren();
	}
	
	private void initHeadline() {
		
		headlineBox = new HBox();
		headlineBox.setId("documentHeadlineBox");
		headlineBox.setAlignment(Pos.CENTER_LEFT);
	}
	
	private void addHeadline() {
		
		this.getChildren().add(headlineBox);
	}
	
	private void initHeadlineComponents() {
		
		documentPathTextField = new ErrorableTextField();
		documentPathTextField.setPromptText(Language.get(COMPONENT_DOCUMENTAREA_DOCUMENTPATHFIELD_PROMPTTEXT));
		documentPathTextField.setDisable(true);
		
		ImageView imageGoBackButton = new ImageView(getClass().getResource("/de/homedms/images/back-icon.png").toString());
		imageGoBackButton.fitHeightProperty().bind(documentPathTextField.heightProperty());
		imageGoBackButton.setPreserveRatio(true);
		
		goBackButton = new Button();
		goBackButton.setGraphic(imageGoBackButton);
		goBackButton.setStyle("-fx-background-color: transparent;");
		
		ImageView imageEditDocumentPathButton = new ImageView(getClass().getResource("/de/homedms/images/edit-icon.png").toString());
		imageEditDocumentPathButton.fitHeightProperty().bind(documentPathTextField.heightProperty());
		imageEditDocumentPathButton.setPreserveRatio(true);
		
		editDocumentPathButton = new Button();
		editDocumentPathButton.setGraphic(imageEditDocumentPathButton);
		editDocumentPathButton.setStyle("-fx-background-color: transparent;");
		
		ImageView imageReloadButton = new ImageView(getClass().getResource("/de/homedms/images/reload-icon.png").toString());
		imageReloadButton.fitHeightProperty().bind(documentPathTextField.heightProperty());
		imageReloadButton.setPreserveRatio(true);
		
		reloadButton = new Button();
		reloadButton.setGraphic(imageReloadButton);
		reloadButton.setStyle("-fx-background-color: transparent;");
	}
	
	private void addComponentsToHeadlineGrid() {
		
		headlineBox.getChildren().add(goBackButton);
		headlineBox.getChildren().add(documentPathTextField);
		headlineBox.getChildren().add(editDocumentPathButton);
		headlineBox.getChildren().add(reloadButton);
	}
	
	@SuppressWarnings("static-access")
	private void adjustHeadlineLayout() {
		
		headlineBox.getChildren().forEach(child -> headlineBox.setHgrow(child, Priority.ALWAYS));
	}
	
	private void initRecordScrollPane() {
		
		recordScrollPane = new ScrollPane() {
			//Avoid focus of ScrollPane
			@Override
			public void requestFocus() { }
		};
		recordScrollPane.setFitToWidth(true);
		recordScrollPane.prefViewportWidthProperty().bind(recordScrollPane.widthProperty());
		recordScrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		
		recordScrollPaneContentBox = new VBox();
		recordScrollPane.setContent(recordScrollPaneContentBox);
		
		// -------------------------------------------------------------
		/*
		File rootDirectory = new File("L:\\Studium\\3. Studienjahr\\Studienarbeit");
		for (File file : rootDirectory.listFiles()) {
			FileRecordView fileRecord = new FileRecordView(file);
			Tooltip.install(fileRecord, new Tooltip(file.getName()));
			recordScrollPaneContentBox.getChildren().add(fileRecord);
		}
		*/
		// -------------------------------------------------------------
	}
	
	private void addRecordScrollPane() {
		
		this.getChildren().add(recordScrollPane);
	}
	
}
