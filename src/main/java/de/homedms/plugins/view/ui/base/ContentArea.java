package de.homedms.plugins.view.ui.base;

import de.homedms.plugins.view.ui.detailarea.DetailArea;
import de.homedms.plugins.view.ui.docarea.DocumentArea;
import de.homedms.plugins.view.util.ColumnConstraintsBuilder;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

public class ContentArea extends GridPane {
	
	//Accessible Components
	
	private DocumentArea documentArea;
	private DetailArea detailArea;
	
	
	public ContentArea() {
		
		initContentGrid();
		initContent();
		addContent();
		
	}
	
	
	public DocumentArea getDocumentArea() {
		
		return documentArea;
	}
	
	public DetailArea getDetailArea() {
		
		return detailArea;
	}
	
	public void setDetailArea(DetailArea detailArea) {
		
		this.removeDetailArea();
		this.detailArea = detailArea;
		this.addColumn(1, detailArea);
	}
	
	public void removeDetailArea() {
		
		this.getChildren().removeIf(node -> GridPane.getColumnIndex(node) == 1);
	}
	
	private void initContentGrid() {
		
		this.getColumnConstraints().add(new ColumnConstraintsBuilder().hGrowAlways().percentWidth(30).build());
		this.getColumnConstraints().add(new ColumnConstraintsBuilder().hGrowAlways().percentWidth(70).build());
		
		RowConstraints rowConstraints = new RowConstraints();
		rowConstraints.setPercentHeight(100);
		rowConstraints.setVgrow(Priority.ALWAYS);
		this.getRowConstraints().add(rowConstraints);
		
		this.setPadding(new Insets(5));
	}
	
	private void initContent() {
		
		documentArea = new DocumentArea();
	}
	
	private void addContent() {
		
		this.addColumn(0, documentArea);
	}
	
}
