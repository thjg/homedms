package de.homedms.plugins.view.ui.base;

import static de.homedms.application.util.Language.*;

import de.homedms.application.util.Language;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ApplicationView extends Stage {
	
	private VBox rootPane;
	private Scene rootScene;
	
	private ContentArea contentArea;
	
	
	public ApplicationView() {
		
		initializeStage();
		
		initContentArea();
		addContentArea();
		
		rootScene.getStylesheets().add(getClass().getResource("/stylesheet.css").toString());
		
		this.setScene(rootScene);
		this.show();
		
	}
	
	
	public ContentArea getContentArea() {
		
		return contentArea;
	}
	
	private void initializeStage() {
		
		this.setTitle(Language.get(TITLE));
		this.getIcons().add(new Image(getClass().getResource("/de/homedms/images/home-icon.png").toString()));
		
		this.setWidth(1280);
		this.setHeight(720);
		
		rootPane = new VBox();
		rootScene = new Scene(rootPane);
	}
	
	@SuppressWarnings("static-access")
	private void initContentArea() {
		
		contentArea = new ContentArea();
		rootPane.setVgrow(contentArea, Priority.ALWAYS);
	}
	
	private void addContentArea() {
		
		rootPane.getChildren().add(contentArea);
	}
}
