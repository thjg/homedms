package de.homedms.plugins.view.control;

import java.io.File;
import java.util.UUID;

import de.homedms.application.DMSApplicationService;
import de.homedms.application.util.EventType;
import de.homedms.application.util.IObserver;
import de.homedms.application.util.Language;
import de.homedms.domain.FileRecordEntity;
import de.homedms.domain.FileRecordType;
import de.homedms.plugins.view.ui.base.ApplicationView;
import de.homedms.plugins.view.ui.components.ErrorableTextField;
import de.homedms.plugins.view.ui.docarea.FileRecordView;
import de.homedms.plugins.view.util.AlertBuilder;

import static de.homedms.application.util.Language.*;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class ApplicationUIControl extends AUIControl implements IObserver {
	
	private ErrorableTextField searchTextField;
	private MenuButton selectionButton;
	
	private Button goBackButton;
	private ErrorableTextField documentPathTextField;
	private Button editDocumentPathButton;
	private Button reloadButton;
	
	private ObservableList<Node> fileRecordViews;
	
	
	public ApplicationUIControl(ApplicationView applicationView, DMSApplicationService applicationService) {
		super(applicationView, applicationService);
		
		initReferenceComponents();
		initUIListeners();
	}
	
	
	private void initReferenceComponents() {
		
		searchTextField = applicationView.getContentArea().getDocumentArea().getSearchTextField();
		selectionButton = applicationView.getContentArea().getDocumentArea().getSelectionButton();
		
		goBackButton = applicationView.getContentArea().getDocumentArea().getDocumentRecordArea().getGoBackButton();
		documentPathTextField = applicationView.getContentArea().getDocumentArea().getDocumentRecordArea().getDocumentPathTextField();
		editDocumentPathButton = applicationView.getContentArea().getDocumentArea().getDocumentRecordArea().getEditDocumentPathButton();
		reloadButton = applicationView.getContentArea().getDocumentArea().getDocumentRecordArea().getReloadButton();
		
		fileRecordViews = applicationView.getContentArea().getDocumentArea().getDocumentRecordArea().getFileRecordViews();
	}
	
	private void initUIListeners() {
		
		searchTextField.textProperty().addListener(getSearchTextFieldChangeListener());
		selectionButton.getItems().forEach(menuItem -> menuItem.setOnAction(getSelectionButtonItemEventHandler(menuItem.getText())));
		
		goBackButton.setOnAction(getGoBackButtonPressedEventHandler());
		documentPathTextField.setOnKeyReleased(getPathTextFieldKeyEventHandler());
		editDocumentPathButton.setOnAction(getEditDocumentPathButtonPressedEventHandler());
		reloadButton.setOnAction(getReloadButtonPressedEventHandler());
	}

	
	/**
	 * SearchTextField-ChangeListener
	 * @return
	 */
	private ChangeListener<String> getSearchTextFieldChangeListener() {
		
		return new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				
				applicationService.submitSearchTerm(newValue);
			}
		};
	}
	
	private EventHandler<ActionEvent> getSelectionButtonItemEventHandler(String text) {
		
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				ImageView imageSelectionButton;
				if (text.equals("-")) {
					applicationService.submitSelectionFileRecordType(null);
					imageSelectionButton = new ImageView(getClass().getResource("/de/homedms/images/filter-icon.png").toString());
				} else {
					applicationService.submitSelectionFileRecordType(FileRecordType.of(text));
					imageSelectionButton = new ImageView(getClass().getResource("/de/homedms/images/filter-icon-filled.png").toString());
				}
				imageSelectionButton.fitHeightProperty().bind(searchTextField.heightProperty());
				imageSelectionButton.setPreserveRatio(true);
				selectionButton.setGraphic(imageSelectionButton);
			}
		};
	}
	
	/**
	 * GoBackButton-EventHandler
	 * @return
	 */
	private EventHandler<ActionEvent> getGoBackButtonPressedEventHandler() {
		
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				File upperDirectory = new File(documentPathTextField.getText()).getParentFile();
				if (upperDirectory != null) {
					submitPathToModel(upperDirectory.getAbsolutePath());
				} else {
					submitPathToModel("");
				}
			}
		};
	}
	
	/**
	 * PathTextField-EventHandler
	 * @return
	 */
	private EventHandler<KeyEvent> getPathTextFieldKeyEventHandler() {
		
		return new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				
				if (event.getCode() == KeyCode.ENTER) {
					submitPathToModel(documentPathTextField.getText());
				} else if (event.getCode() == KeyCode.ESCAPE) {
					restorePreviousPath();
				}
			}
		};
	}
	
	/**
	 * EditDocumentPathButton-EventHandler
	 * @return
	 */
	private EventHandler<ActionEvent> getEditDocumentPathButtonPressedEventHandler() {
		
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				if (!documentPathTextField.isDisable()) {
					restorePreviousPath();
				} else {
					documentPathTextField.setDisable(false);
				}
			}
		};
	}
	
	/**
	 * ReloadButton-EventHandler
	 * @return
	 */
	private EventHandler<ActionEvent> getReloadButtonPressedEventHandler() {

		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				submitPathToModel(documentPathTextField.getText());
			}
		};
	}
	
	/**
	 * FileRecordView-EventHandler
	 * @return
	 */
	private EventHandler<MouseEvent> getFileRecordViewEventHandler() {
		
		return new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				
				UUID uuid = ((FileRecordView) event.getSource()).getUUID();
				try {
					if (event.getClickCount() == 1) {
						applicationService.getFileRecordSelectionService().changeSelectionOfFileRecordByUUID(uuid);
					} else if (event.getClickCount() == 2) {
						applicationService.getFileRecordSelectionService().setSelectionOfFileRecordByUUID(uuid, true);
						applicationService.enterDirectoryOrOpenFile(uuid);
					}
				} catch (Exception exception) {
					new AlertBuilder(AlertType.ERROR).setText(exception.getMessage()).show();
				}
			}
		};
	}
	
	public void submitPathToModel(String path) {
		
		try {
			applicationService.submitDocumentPath(path);
		} catch (Exception exception) {
			if (exception.getMessage().equals(Language.get(ERRORMESSAGE_INVALIDDOCUMENTPATH))) {
				documentPathTextField.setError(exception.getMessage());
			} else if (exception.getMessage().equals(Language.get(ERRORMESSAGE_DOCUMENTPATHCACHINGFAILED))) {
				new AlertBuilder(AlertType.ERROR).setText(Language.get(ERRORMESSAGE_DOCUMENTPATHCACHINGFAILED)).show();
			}
		}
	}
	
	private void restorePreviousPath() {
		
		documentPathTextField.setText(applicationService.getDocumentPath());
		documentPathTextField.setDisable(true);
		documentPathTextField.clearError();
	}


	@Override
	public void handle(EventType event) {
		
		if (event == EventType.FILE_RECORD_SELECTED) {
			UUID selectedUUID = applicationService.getFileRecordSelectionService().getCurrentlySelectedFileRecordEntity().get().getUUID();
			((FileRecordView) fileRecordViews.stream().filter(fileRecordView -> ((FileRecordView) fileRecordView).getUUID().equals(selectedUUID)).findFirst().get()).setSelected(true);
		} else if (event == EventType.FILE_RECORD_UNSELECTED) {
			fileRecordViews.stream().filter(fileRecordView -> ((FileRecordView) fileRecordView).isSelected()).findFirst().ifPresent(fileRecordView -> ((FileRecordView) fileRecordView).setSelected(false));
		} else if (event == EventType.FILE_RECORD_REBUILD) {
			fileRecordViews.clear();
			for (FileRecordEntity fileRecordEntity : applicationService.getFilteredFileRecordEntities()) {
				FileRecordView fileRecordView = new FileRecordView(fileRecordEntity.getFile(), fileRecordEntity.getUUID());
				fileRecordView.setSelected(fileRecordEntity.isSelected());
				fileRecordView.setOnMouseClicked(getFileRecordViewEventHandler());
				fileRecordViews.add(fileRecordView);
			}
		} else if (event == EventType.DOCUMENT_PATH_UPDATE) {
			documentPathTextField.setText(applicationService.getDocumentPath());
			documentPathTextField.setDisable(true);
			documentPathTextField.clearError();
		}
	}
}
