package de.homedms.plugins.view.control;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import de.homedms.application.DMSApplicationService;
import de.homedms.application.util.EventType;
import de.homedms.application.util.IObserver;
import de.homedms.application.util.Language;
import de.homedms.domain.ADetailRecord;
import de.homedms.domain.FileRecordEntity;
import de.homedms.domain.FileRecordType;
import de.homedms.domain.detailrecords.AccountStatementDetailRecordVO;
import de.homedms.domain.detailrecords.BillDetailRecordVO;
import de.homedms.domain.detailrecords.StandardDetailRecordVO;
import de.homedms.plugins.view.ui.base.ApplicationView;
import de.homedms.plugins.view.ui.base.ContentArea;
import de.homedms.plugins.view.ui.detailarea.DetailArea;
import de.homedms.plugins.view.ui.detailarea.detailfields.AccountStatementDetailFieldsArea;
import de.homedms.plugins.view.ui.detailarea.detailfields.BillDetailFieldsArea;
import de.homedms.plugins.view.ui.detailarea.detailfields.DetailFieldsArea;
import de.homedms.plugins.view.util.AlertBuilder;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Alert.AlertType;

public class DetailUIControl extends AUIControl implements IObserver {
	
	private ContentArea contentArea;
	
	private Button openFileButton;
	
	private ComboBox<String> fileTypeComboBox;
	
	private Button saveButton;
	private Button resetButton;
	private Button deleteButton;
	
	public DetailUIControl(ApplicationView applicationView, DMSApplicationService applicationService) {
		super(applicationView, applicationService);
		
		contentArea = applicationView.getContentArea();
	}
	
	private void initReferenceComponents() {
		
		openFileButton = contentArea.getDetailArea().getOpenFileButton();
		
		fileTypeComboBox = contentArea.getDetailArea().getDetailFieldsArea().getDataTypeComboBox();
		
		saveButton = contentArea.getDetailArea().getFootArea().getSaveButton();
		resetButton = contentArea.getDetailArea().getFootArea().getResetButton();
		deleteButton = contentArea.getDetailArea().getFootArea().getDeleteButton();
	}
	
	private void initUIListeners() {
		
		openFileButton.setOnAction(getOpenFileButtonPressedEventHandler());
		
		fileTypeComboBox.valueProperty().addListener(getFileTypeComboBoxChangedListener());
		
		saveButton.setOnAction(getSaveButtonPressedEventHandler());
		resetButton.setOnAction(getResetButtonPressedEventHandler());
		deleteButton.setOnAction(getDeleteButtonPressedEventHandler());
	}
	
	private EventHandler<ActionEvent> getOpenFileButtonPressedEventHandler() {
		
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				try {
					applicationService.openCurrentlySelectedFile();
				} catch (Exception exception) {
					new AlertBuilder(AlertType.ERROR).setText(exception.getMessage()).show();
				}
			}
		};
	}
	
	private ChangeListener<String> getFileTypeComboBoxChangedListener() {
		
		return new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				applicationService.changeTypeOfDetailRecordForEditing(FileRecordType.of(newValue));
			}
		};
	}
	
	private EventHandler<ActionEvent> getSaveButtonPressedEventHandler() {
		
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				DetailFieldsArea detailFieldsArea = contentArea.getDetailArea().getDetailFieldsArea();
				ADetailRecord tempRecord = applicationService.getDetailRecordForEditing().get();
				if (tempRecord.getFileRecordType() == FileRecordType.STANDARD) {
					String notes = detailFieldsArea.getNotesTextArea().getText();
					applicationService.setDetailRecordForEditing(new StandardDetailRecordVO(tempRecord.getDirectoryPath(), tempRecord.getName(), Optional.of(notes)));
				} else if (tempRecord.getFileRecordType() == FileRecordType.BILL) {
					String notes = detailFieldsArea.getNotesTextArea().getText();
					LocalDate billDate = ((BillDetailFieldsArea) detailFieldsArea).getBillDatePicker().getValue();
					LocalDate payDate = ((BillDetailFieldsArea) detailFieldsArea).getPayDatePicker().getValue();
					applicationService.setDetailRecordForEditing(new BillDetailRecordVO(tempRecord.getDirectoryPath(), tempRecord.getName(), Optional.of(notes), Optional.ofNullable(billDate), Optional.ofNullable(payDate)));
				} else if (tempRecord.getFileRecordType() == FileRecordType.ACCOUNT_STATEMENT) {
					String notes = contentArea.getDetailArea().getDetailFieldsArea().getNotesTextArea().getText();
					LocalDate dateOfIssue = ((AccountStatementDetailFieldsArea) detailFieldsArea).getAccountStatementDatePicker().getValue();
					String bank = ((AccountStatementDetailFieldsArea) detailFieldsArea).getBankTextField().getText();
					applicationService.setDetailRecordForEditing(new AccountStatementDetailRecordVO(tempRecord.getDirectoryPath(), tempRecord.getName(), Optional.of(notes), Optional.ofNullable(dateOfIssue), Optional.ofNullable(bank)));
				}
				applicationService.saveDetailRecordForEditing();
			}
		};
	}
	
	private EventHandler<ActionEvent> getResetButtonPressedEventHandler() {
		
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				applicationService.resetDetailRecordForEditing();
			}
		};
	}
	
	private EventHandler<ActionEvent> getDeleteButtonPressedEventHandler() {
		
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				applicationService.deleteDetailRecord();
			}
		};
	}

	@Override
	public void handle(EventType event) {
		
		if (event == EventType.FILE_RECORD_SELECTED) {
			FileRecordEntity selectedFileRecordEntity = applicationService.getFileRecordSelectionService().getCurrentlySelectedFileRecordEntity().get();
			
			if (selectedFileRecordEntity.getFile().isDirectory()) {
				return;
			}
			
			ADetailRecord detailRecord = applicationService.getDetailRecordForEditing().get();
			contentArea.setDetailArea(new DetailArea(detailRecord.getFileRecordType()));
			
			initReferenceComponents();
			initUIListeners();
			
			contentArea.getDetailArea().getFilenameTextField().setText(selectedFileRecordEntity.getFileRecordMetaVO().getName());
			contentArea.getDetailArea().getDetailFieldsArea().getChangeDateTextField().setText(LocalDate.ofEpochDay(TimeUnit.MILLISECONDS.toDays(selectedFileRecordEntity.getFileRecordMetaVO().getLastModified())).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")).toString());
			contentArea.getDetailArea().getDetailFieldsArea().getDataSizeTextField().setText(selectedFileRecordEntity.getFileRecordMetaVO().getFileSizeInReadableFormat());
			contentArea.getDetailArea().getDetailFieldsArea().getDataTypeComboBox().setValue(Language.get(detailRecord.getFileRecordType()));
			
			if (detailRecord.getFileRecordType() == FileRecordType.STANDARD) {
				contentArea.getDetailArea().getDetailFieldsArea().getNotesTextArea().setText("");
				
				StandardDetailRecordVO standardDetailRecord = (StandardDetailRecordVO) detailRecord;
				if (standardDetailRecord.getNotes().isPresent()) {
					contentArea.getDetailArea().getDetailFieldsArea().getNotesTextArea().setText(standardDetailRecord.getNotes().get());
				}
			} else if (detailRecord.getFileRecordType() == FileRecordType.BILL) {
				contentArea.getDetailArea().getDetailFieldsArea().getNotesTextArea().setText("");
				((BillDetailFieldsArea) contentArea.getDetailArea().getDetailFieldsArea()).getBillDatePicker().setValue(null);
				((BillDetailFieldsArea) contentArea.getDetailArea().getDetailFieldsArea()).getPayDatePicker().setValue(null);
				
				BillDetailRecordVO billDetailRecord = (BillDetailRecordVO) detailRecord;
				if (billDetailRecord.getNotes().isPresent()) {
					contentArea.getDetailArea().getDetailFieldsArea().getNotesTextArea().setText(billDetailRecord.getNotes().get());
				}
				if (billDetailRecord.getBillDate().isPresent()) {
					((BillDetailFieldsArea) contentArea.getDetailArea().getDetailFieldsArea()).getBillDatePicker().setValue(billDetailRecord.getBillDate().get());
				}
				if (billDetailRecord.getPayDate().isPresent()) {
					((BillDetailFieldsArea) contentArea.getDetailArea().getDetailFieldsArea()).getPayDatePicker().setValue(billDetailRecord.getPayDate().get());
				}
			} else if (detailRecord.getFileRecordType() == FileRecordType.ACCOUNT_STATEMENT) {
				contentArea.getDetailArea().getDetailFieldsArea().getNotesTextArea().setText("");
				((AccountStatementDetailFieldsArea) contentArea.getDetailArea().getDetailFieldsArea()).getAccountStatementDatePicker().setValue(null);
				((AccountStatementDetailFieldsArea) contentArea.getDetailArea().getDetailFieldsArea()).getBankTextField().setText("");
				
				AccountStatementDetailRecordVO accountStatementDetailRecord = (AccountStatementDetailRecordVO) detailRecord;
				if (accountStatementDetailRecord.getNotes().isPresent()) {
					contentArea.getDetailArea().getDetailFieldsArea().getNotesTextArea().setText(accountStatementDetailRecord.getNotes().get());
				}
				if (accountStatementDetailRecord.getDateOfIssue().isPresent()) {
					((AccountStatementDetailFieldsArea) contentArea.getDetailArea().getDetailFieldsArea()).getAccountStatementDatePicker().setValue(accountStatementDetailRecord.getDateOfIssue().get());
				}
				if (accountStatementDetailRecord.getBank().isPresent()) {
					((AccountStatementDetailFieldsArea) contentArea.getDetailArea().getDetailFieldsArea()).getBankTextField().setText(accountStatementDetailRecord.getBank().get());
				}
			}
		} else if (event == EventType.FILE_RECORD_UNSELECTED) {
			contentArea.removeDetailArea();
		}
	}
}
