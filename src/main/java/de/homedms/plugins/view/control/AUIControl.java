package de.homedms.plugins.view.control;

import de.homedms.application.DMSApplicationService;
import de.homedms.plugins.view.ui.base.ApplicationView;

public abstract class AUIControl {
	
	protected ApplicationView applicationView;
	protected DMSApplicationService applicationService;
	
	public AUIControl(ApplicationView applicationView, DMSApplicationService applicationService) {
		
		this.applicationView = applicationView;
		this.applicationService = applicationService;
	}
	
}
