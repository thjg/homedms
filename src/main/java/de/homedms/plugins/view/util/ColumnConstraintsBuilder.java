package de.homedms.plugins.view.util;

import javafx.geometry.HPos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.Priority;

public class ColumnConstraintsBuilder {
	
	private ColumnConstraints colConstraints;
	
	
	public ColumnConstraintsBuilder() {
		
		colConstraints = new ColumnConstraints();
	}
	
	public ColumnConstraints build() {
		
		return colConstraints;
	}
	
	
	public ColumnConstraintsBuilder hGrowAlways() {
		
		colConstraints.setHgrow(Priority.ALWAYS);
		return this;
	}
	
	public ColumnConstraintsBuilder percentWidth(int percent) {
		
		colConstraints.setPercentWidth(percent);
		return this;
	}
	
	public ColumnConstraintsBuilder hAlignmentLeft() {
		
		colConstraints.setHalignment(HPos.LEFT);
		return this;
	}
	
	public ColumnConstraintsBuilder hAlignmentCenter() {
		
		colConstraints.setHalignment(HPos.CENTER);
		return this;
	}
	
	public ColumnConstraintsBuilder hAlignmentRight() {
		
		colConstraints.setHalignment(HPos.RIGHT);
		return this;
	}
	
}
