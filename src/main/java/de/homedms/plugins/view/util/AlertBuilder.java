package de.homedms.plugins.view.util;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class AlertBuilder {
	
	private Alert alert;
	private TextArea content;
	
	public AlertBuilder(AlertType alertType) {
		
		alert = new Alert(alertType);
		alert.setTitle("HomeDMS");
		Stage dialogStage = (Stage) alert.getDialogPane().getScene().getWindow();
		dialogStage.getIcons().add(new Image(getClass().getResource("/de/homedms/images/home-icon.png").toString()));
		
		content = new TextArea();
		content.setEditable(false);
	}
	
	public AlertBuilder setText(String text) {
		
		content.setText(text);
		return this;
	}
	
	public void show() {
		
		alert.getDialogPane().setContent(content);
		alert.show();
	}
}
