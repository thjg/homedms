package de.homedms.application.subservices;

import java.util.Optional;
import java.util.UUID;

import de.homedms.application.util.EventType;
import de.homedms.application.util.IObservable;
import de.homedms.domain.FileRecordEntity;
import de.homedms.domain.IFileRecordRepository;
import de.homedms.domain.detailrecords.StandardDetailRecordVO;

public class FileRecordSelectionService {
	
	private final IFileRecordRepository fileRecordRepository;
	private final IObservable notificationProvider;
	
	public FileRecordSelectionService(IFileRecordRepository fileRecordRepository, IObservable notificationProvider) {
		
		this.fileRecordRepository = fileRecordRepository;
		this.notificationProvider = notificationProvider;
	}
	
	public Optional<FileRecordEntity> getCurrentlySelectedFileRecordEntity() {
		
		return fileRecordRepository.getFileRecordEntities().stream().filter(fileRecordEntity -> fileRecordEntity.isSelected()).findFirst();
	}
	
	public void changeSelectionOfFileRecordByUUID(UUID uuid) {
		
		FileRecordEntity requestedEntity = fileRecordRepository.getFileRecordEntityByUUID(uuid);
		boolean wasSelected = requestedEntity.isSelected();
		
		unselectCurrentlySelectedFileRecordEntity();
		
		if (!wasSelected) {
			requestedEntity.setSelectedState(true);
			if (requestedEntity.getDetailRecord().isPresent()) {
				fileRecordRepository.setDetailRecordForEditing(Optional.of(requestedEntity.getDetailRecord().get().copy()));
			} else {
				fileRecordRepository.setDetailRecordForEditing(Optional.of(StandardDetailRecordVO.createDefault(requestedEntity.getFileRecordMetaVO().getDirectoryPath(), requestedEntity.getFileRecordMetaVO().getName())));
			}
			notificationProvider.triggerEvent(EventType.FILE_RECORD_SELECTED);
		}
	}
	
	public void setSelectionOfFileRecordByUUID(UUID uuid, boolean select) {
		
		FileRecordEntity requestedEntity = fileRecordRepository.getFileRecordEntityByUUID(uuid);
		boolean currentlySelected = requestedEntity.isSelected();
		
		if (currentlySelected && !select) {
			unselectCurrentlySelectedFileRecordEntity();
		} else if (!currentlySelected && select) {
			changeSelectionOfFileRecordByUUID(uuid);
		}
	}
	
	public void unselectCurrentlySelectedFileRecordEntity() {
		
		Optional<FileRecordEntity> optFileRecordEntity = getCurrentlySelectedFileRecordEntity();
		if (optFileRecordEntity.isPresent()) {
			optFileRecordEntity.get().setSelectedState(false);
			fileRecordRepository.setDetailRecordForEditing(Optional.empty());
			notificationProvider.triggerEvent(EventType.FILE_RECORD_UNSELECTED);
		}
	}
}
