package de.homedms.application.subservices;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.homedms.application.util.EventType;
import de.homedms.application.util.IObservable;
import de.homedms.domain.FileRecordEntity;
import de.homedms.domain.FileRecordType;
import de.homedms.domain.IFileRecordRepository;

public class FileRecordDeterminationService {
	
	private IFileRecordRepository fileRecordRepository;
	private IObservable notificationProvider;
	private String documentPath;
	private Optional<FileRecordType> selectionFileRecordType;
	
	public FileRecordDeterminationService(IFileRecordRepository fileRecordRepository, IObservable notificationProvider) {
		
		this.fileRecordRepository = fileRecordRepository;
		this.notificationProvider = notificationProvider;
		this.documentPath = fileRecordRepository.getDocumentPath();
		this.selectionFileRecordType = Optional.ofNullable(fileRecordRepository.getSelectionFileRecordType());
	}
	
	public void startDeterminationProcess() {
		
		this.documentPath = fileRecordRepository.getDocumentPath();
		fileRecordRepository.getFileRecordEntities().clear();
		
		List<FileRecordEntity> directoryFileRecordEntities = new ArrayList<>();
		List<FileRecordEntity> fileFileRecordEntities = new ArrayList<>();
		
		if (documentPath.equals("")) {
			//Root-Directories requested
			for (File file : File.listRoots()) {
				directoryFileRecordEntities.add(new FileRecordEntity(file, null));
			}
		} else {
			//Normal File Determination Process
			File directory = new File(documentPath);
			for (File file : directory.listFiles()) {
				FileRecordEntity fileRecordEntity = new FileRecordEntity(file, null);
				if (file.isDirectory()) {
					directoryFileRecordEntities.add(fileRecordEntity);
				} else {
					fileFileRecordEntities.add(fileRecordEntity);
				}
			}
		}
		
		fileRecordRepository.getFileRecordEntities().addAll(directoryFileRecordEntities);
		fileRecordRepository.getFileRecordEntities().addAll(fileFileRecordEntities);
		
		startFilteredDeterminationProcess();
	}
	
	public void startFilteredDeterminationProcess() {
		
		this.documentPath = fileRecordRepository.getDocumentPath();
		this.selectionFileRecordType = Optional.ofNullable(fileRecordRepository.getSelectionFileRecordType());
		fileRecordRepository.getFilteredFileRecordEntities().clear();
		fileRecordRepository.getFilteredFileRecordEntities().addAll(fileRecordRepository.getFileRecordEntities());
		
		if (documentPath.equals("")) {
			//The Path should be used for the Search in Root-Directories
			fileRecordRepository.getFilteredFileRecordEntities().removeIf(
					fileRecordEntity -> !fileRecordEntity.getFile().getPath().toUpperCase().contains(fileRecordRepository.getSearchTerm().toUpperCase()));
		} else {
			fileRecordRepository.getFilteredFileRecordEntities().removeIf(
					fileRecordEntity -> !fileRecordEntity.getFile().getName().toUpperCase().contains(fileRecordRepository.getSearchTerm().toUpperCase()));
		}
		if (selectionFileRecordType.isPresent()) {
			fileRecordRepository.getFilteredFileRecordEntities().removeIf(
					fileRecordEntity -> !fileRecordEntity.getDetailRecord().isPresent() ||
					fileRecordEntity.getDetailRecord().isPresent() && fileRecordEntity.getDetailRecord().get().getFileRecordType() != selectionFileRecordType.get());
		}
		
		notificationProvider.triggerEvent(EventType.FILE_RECORD_REBUILD);
	}
}
