package de.homedms.application.subservices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService {
	
	public static final String PROPERTY_LAST_PATH = "last_path";
	
	private static String PATH_PROPERTIES;
	
	private static Properties properties = new Properties();
	private static Properties languageProperties = new Properties();
	
	public static void init(String propertiesPath, InputStream languagePropertiesInputStream) {
		
		PATH_PROPERTIES = propertiesPath;
		
		initProperties();
		initLanguageProperties(languagePropertiesInputStream);
	}
	
	public static Properties getProperties() {
		
		return properties;
	}
	
	public static void saveProperties() {
		
		try {
			properties.store(new FileOutputStream(PATH_PROPERTIES), "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Properties getLanguageProperties() {
		
		return languageProperties;
	}
	
	private static void initProperties() {
		
		try {
			if (!new File(PATH_PROPERTIES).exists()) {
				new File(PATH_PROPERTIES).createNewFile();
			}
			properties.load(new FileInputStream(PATH_PROPERTIES));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void initLanguageProperties(InputStream languagePropertiesInputStream) {
		
		try {
			languageProperties.load(languagePropertiesInputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
