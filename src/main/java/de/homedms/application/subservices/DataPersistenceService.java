package de.homedms.application.subservices;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;

import de.homedms.application.util.IDatabase;
import de.homedms.domain.ADetailRecord;
import de.homedms.domain.FileRecordType;
import de.homedms.domain.detailrecords.AccountStatementDetailRecordVO;
import de.homedms.domain.detailrecords.BillDetailRecordVO;
import de.homedms.domain.detailrecords.StandardDetailRecordVO;

public class DataPersistenceService {
	
	//TABLES
	public static final String TABLE_BASE = "base";
	public static final String COL_BASE_DIRECTORYPATH = "directoryPath";
	public static final String COL_BASE_NAME = "name";
	public static final String COL_BASE_TYPE = "type";
	
	public static final String TABLE_STANDARD = "fileStandard";
	public static final String COL_STANDARD_DIRECTORYPATH = "directoryPath";
	public static final String COL_STANDARD_NAME = "name";
	public static final String COL_STANDARD_NOTES = "notes";
	
	public static final String TABLE_BILL = "fileBill";
	public static final String COL_BILL_DIRECTORYPATH = "directoryPath";
	public static final String COL_BILL_NAME = "name";
	public static final String COL_BILL_BILLDATE = "billDate";
	public static final String COL_BILL_PAYDATE = "payDate";
	
	public static final String TABLE_ACCOUNT_STATEMENT = "fileAccountStatement";
	public static final String COL_ACCOUNT_STATEMENT_DIRECTORYPATH = "directoryPath";
	public static final String COL_ACCOUNT_STATEMENT_NAME = "name";
	public static final String COL_ACCOUNT_STATEMENT_DATE = "date";
	public static final String COL_ACCOUNT_STATEMENT_BANK = "bank";
	
	private IDatabase database;
	
	public DataPersistenceService(IDatabase database) {
		
		this.database = database;
	}
	
	public Optional<ADetailRecord> getDetailRecordDataFor(String directoryPath, String name) {
		
		try {
			StringBuilder selectSQLBuilder = new StringBuilder()
					.append("SELECT * FROM ")
					.append(TABLE_BASE);
			selectSQLBuilder = appendLeftJoinStandard(selectSQLBuilder);
			selectSQLBuilder = appendLeftJoinBill(selectSQLBuilder);
			selectSQLBuilder = appendLeftJoinAccountStatement(selectSQLBuilder);
			selectSQLBuilder.append(" WHERE ")
					.append(TABLE_BASE + "." + COL_BASE_DIRECTORYPATH).append(" = ? AND ")
					.append(TABLE_BASE + "." + COL_BASE_NAME).append(" = ?");
			String selectSQL = selectSQLBuilder.toString();
			
			ResultSet resultSet = database.executeSelect(selectSQL, directoryPath, name);
			if (resultSet.next()) {
				FileRecordType fileRecordType = FileRecordType.of(resultSet.getString(COL_BASE_TYPE));
				
				if (fileRecordType == FileRecordType.STANDARD) {
					return Optional.of(new StandardDetailRecordVO(directoryPath, name,
							Optional.ofNullable(resultSet.getString(COL_STANDARD_NOTES))));
				} else if (fileRecordType == FileRecordType.BILL) {
					return Optional.of(new BillDetailRecordVO(directoryPath, name,
							Optional.ofNullable(resultSet.getString(COL_STANDARD_NOTES)),
							createLocalDateOptionalBy(resultSet.getString(COL_BILL_BILLDATE)),
							createLocalDateOptionalBy(resultSet.getString(COL_BILL_PAYDATE))));
				} else if (fileRecordType == FileRecordType.ACCOUNT_STATEMENT) {
					return Optional.of(new AccountStatementDetailRecordVO(directoryPath, name,
							Optional.ofNullable(resultSet.getString(COL_STANDARD_NOTES)),
							createLocalDateOptionalBy(resultSet.getString(COL_ACCOUNT_STATEMENT_DATE)),
							Optional.ofNullable(resultSet.getString(COL_ACCOUNT_STATEMENT_BANK))));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}
	
	public void saveDetailRecordDataFor(ADetailRecord detailRecord) {
		
		String directoryPath = detailRecord.getDirectoryPath();
		String name = detailRecord.getName();
		FileRecordType fileRecordType = detailRecord.getFileRecordType();
		
		deleteDetailRecordDataFor(directoryPath, name);
		insertBaseDetailRecordDataFor(detailRecord);
		
		if (fileRecordType == FileRecordType.STANDARD) {
			insertStandardDetailRecordDataFor((StandardDetailRecordVO) detailRecord);
		} else if (fileRecordType == FileRecordType.BILL) {
			insertBillDetailRecordDataFor((BillDetailRecordVO) detailRecord);
		} else if (fileRecordType == FileRecordType.ACCOUNT_STATEMENT) {
			insertAccountStatementDetailRecordDataFor((AccountStatementDetailRecordVO) detailRecord);
		}
	}
	
	public void deleteDetailRecordDataFor(String directoryPath, String name) {
		
		deleteBaseDetailRecordDataFor(directoryPath, name);
		deleteStandardDetailRecordDataFor(directoryPath, name);
		deleteBillDetailRecordDataFor(directoryPath, name);
		deleteAccountStatementDetailRecordDataFor(directoryPath, name);
	}
	
	public void initTables() {
		
		initTable(TABLE_BASE,
				Pair.of(COL_BASE_DIRECTORYPATH, "text"),
				Pair.of(COL_BASE_NAME, "text"),
				Pair.of(COL_BASE_TYPE, "text"));
		initTable(TABLE_STANDARD,
				Pair.of(COL_STANDARD_DIRECTORYPATH, "text"),
				Pair.of(COL_STANDARD_NAME, "text"),
				Pair.of(COL_STANDARD_NOTES, "text"));
		initTable(TABLE_BILL,
				Pair.of(COL_BILL_DIRECTORYPATH, "text"),
				Pair.of(COL_BILL_NAME, "text"),
				Pair.of(COL_BILL_BILLDATE, "text"),
				Pair.of(COL_BILL_PAYDATE, "text"));
		initTable(TABLE_ACCOUNT_STATEMENT,
				Pair.of(COL_ACCOUNT_STATEMENT_DIRECTORYPATH, "text"),
				Pair.of(COL_ACCOUNT_STATEMENT_NAME, "text"),
				Pair.of(COL_ACCOUNT_STATEMENT_DATE, "text"),
				Pair.of(COL_ACCOUNT_STATEMENT_BANK, "text"));
	}
	
	@SafeVarargs
	private void initTable(String tablename, Pair<String, String>... colNameAndType) {
		
		try {
			StringBuilder createBaseTableSQLBuilder = new StringBuilder()
					.append("CREATE TABLE IF NOT EXISTS ")
					.append(tablename);
			
			createBaseTableSQLBuilder.append(" (");
			for (int i = 0; i < colNameAndType.length; i++) {
				createBaseTableSQLBuilder.append(colNameAndType[i].getKey());
				createBaseTableSQLBuilder.append(" ");
				createBaseTableSQLBuilder.append(colNameAndType[i].getValue());
				if (i != colNameAndType.length - 1) {
					createBaseTableSQLBuilder.append(", ");
				}
			}
			createBaseTableSQLBuilder.append(" )");
			
			database.executeCreateTable(createBaseTableSQLBuilder.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void deleteBaseDetailRecordDataFor(String directoryPath, String name) {
		
		try {
			String deleteBaseSQL = new StringBuilder()
					.append("DELETE FROM ").append(TABLE_BASE)
					.append(" WHERE ")
					.append(COL_BASE_DIRECTORYPATH).append(" = ? AND ")
					.append(COL_BASE_NAME).append(" = ?")
					.toString();
			database.executeInsertOrUpdateOrDelete(deleteBaseSQL, directoryPath, name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void deleteStandardDetailRecordDataFor(String directoryPath, String name) {
		
		try {
			String deleteStandardSQL = new StringBuilder()
					.append("DELETE FROM ").append(TABLE_STANDARD)
					.append(" WHERE ")
					.append(COL_STANDARD_DIRECTORYPATH).append(" = ? AND ")
					.append(COL_STANDARD_NAME).append(" = ?")
					.toString();
			
			database.executeInsertOrUpdateOrDelete(deleteStandardSQL, directoryPath, name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void deleteBillDetailRecordDataFor(String directoryPath, String name) {
		
		try {
			String deleteBillSQL = new StringBuilder()
					.append("DELETE FROM ").append(TABLE_BILL)
					.append(" WHERE ")
					.append(COL_BILL_DIRECTORYPATH).append(" = ? AND ")
					.append(COL_BILL_NAME).append(" = ?")
					.toString();
			
			database.executeInsertOrUpdateOrDelete(deleteBillSQL, directoryPath, name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void deleteAccountStatementDetailRecordDataFor(String directoryPath, String name) {
		
		try {
			String deleteAccountStatementSQL = new StringBuilder()
					.append("DELETE FROM ").append(TABLE_ACCOUNT_STATEMENT)
					.append(" WHERE ")
					.append(COL_ACCOUNT_STATEMENT_DIRECTORYPATH).append(" = ? AND ")
					.append(COL_ACCOUNT_STATEMENT_NAME).append(" = ?")
					.toString();
			
			database.executeInsertOrUpdateOrDelete(deleteAccountStatementSQL, directoryPath, name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void insertBaseDetailRecordDataFor(ADetailRecord detailRecord) {
		
		try {
			String insertSQL = new StringBuilder()
					.append("INSERT INTO ").append(TABLE_BASE)
					.append(" VALUES(?, ?, ?)")
					.toString();
			database.executeInsertOrUpdateOrDelete(insertSQL, detailRecord.getDirectoryPath(), detailRecord.getName(), detailRecord.getFileRecordType().toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void insertStandardDetailRecordDataFor(StandardDetailRecordVO standardDetailRecord) {
		
		try {
			String insertSQL = new StringBuilder()
					.append("INSERT INTO ").append(TABLE_STANDARD)
					.append(" VALUES(?, ?, ?)")
					.toString();
			
			database.executeInsertOrUpdateOrDelete(insertSQL, standardDetailRecord.getDirectoryPath(), standardDetailRecord.getName(), standardDetailRecord.getNotes().orElse(null));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void insertBillDetailRecordDataFor(BillDetailRecordVO billDetailRecord) {
		
		try {
			String insertSQL = new StringBuilder()
					.append("INSERT INTO ").append(TABLE_BILL)
					.append(" VALUES(?, ?, ?, ?)")
					.toString();
			
			database.executeInsertOrUpdateOrDelete(insertSQL, billDetailRecord.getDirectoryPath(), billDetailRecord.getName(),
					(billDetailRecord.getBillDate().isPresent() ? billDetailRecord.getBillDate().get().toString() : null),
					(billDetailRecord.getPayDate().isPresent() ? billDetailRecord.getPayDate().get().toString() : null));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void insertAccountStatementDetailRecordDataFor(AccountStatementDetailRecordVO accountStatementDetailRecord) {
		
		try {
			String insertSQL = new StringBuilder()
					.append("INSERT INTO ").append(TABLE_ACCOUNT_STATEMENT)
					.append(" VALUES(?, ?, ?, ?)")
					.toString();
			
			database.executeInsertOrUpdateOrDelete(insertSQL, accountStatementDetailRecord.getDirectoryPath(), accountStatementDetailRecord.getName(),
					(accountStatementDetailRecord.getDateOfIssue().isPresent() ? accountStatementDetailRecord.getDateOfIssue().get().toString() : null),
					accountStatementDetailRecord.getBank().orElse(null));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private StringBuilder appendLeftJoinStandard(StringBuilder dataSQL) {
		
		dataSQL.append(" LEFT JOIN ")
				.append(TABLE_STANDARD)
				.append(" ON ")
				.append(TABLE_BASE + "." + COL_BASE_DIRECTORYPATH).append(" = ").append(TABLE_STANDARD + "." + COL_STANDARD_DIRECTORYPATH).append(" AND ")
				.append(TABLE_BASE + "." + COL_BASE_NAME).append(" = ").append(TABLE_STANDARD + "." + COL_BASE_NAME);
		return dataSQL;
	}
	
	private StringBuilder appendLeftJoinBill(StringBuilder dataSQL) {
		
		dataSQL.append(" LEFT JOIN ")
				.append(TABLE_BILL)
				.append(" ON ")
				.append(TABLE_BASE + "." + COL_BASE_DIRECTORYPATH).append(" = ").append(TABLE_BILL + "." + COL_BILL_DIRECTORYPATH).append(" AND ")
				.append(TABLE_BASE + "." + COL_BASE_NAME).append(" = ").append(TABLE_BILL + "." + COL_BILL_NAME);
		return dataSQL;
	}
	
	private StringBuilder appendLeftJoinAccountStatement(StringBuilder dataSQL) {
		
		dataSQL.append(" LEFT JOIN ")
				.append(TABLE_ACCOUNT_STATEMENT)
				.append(" ON ")
				.append(TABLE_BASE + "." + COL_BASE_DIRECTORYPATH).append(" = ").append(TABLE_ACCOUNT_STATEMENT + "." + COL_ACCOUNT_STATEMENT_DIRECTORYPATH).append(" AND ")
				.append(TABLE_BASE + "." + COL_BASE_NAME).append(" = ").append(TABLE_ACCOUNT_STATEMENT + "." + COL_ACCOUNT_STATEMENT_NAME);
		return dataSQL;
	}
	
	private static Optional<LocalDate> createLocalDateOptionalBy(String dbValue) {
		
		if (dbValue == null || dbValue.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(LocalDate.parse(dbValue));
	}
}
