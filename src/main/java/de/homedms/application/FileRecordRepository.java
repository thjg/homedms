package de.homedms.application;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import de.homedms.application.util.EventType;
import de.homedms.application.util.IObservable;
import de.homedms.application.util.IObserver;
import de.homedms.domain.ADetailRecord;
import de.homedms.domain.FileRecordEntity;
import de.homedms.domain.FileRecordType;
import de.homedms.domain.IFileRecordRepository;

public class FileRecordRepository implements IFileRecordRepository, IObservable {
	
	private HashSet<IObserver> observers;
	
	private List<FileRecordEntity> fileRecordEntities;
	private List<FileRecordEntity> filteredFileRecordEntities;
	
	private String documentPath = "";
	private String searchTerm = "";
	private FileRecordType selectionFileRecordType;
	
	private Optional<ADetailRecord> detailRecordForEditing;
	
	public FileRecordRepository() {
		
		observers = new HashSet<>();
		
		fileRecordEntities = new ArrayList<>();
		filteredFileRecordEntities = new ArrayList<>();
	}
	
	@Override
	public List<FileRecordEntity> getFileRecordEntities() {
		
		return fileRecordEntities;
	}

	@Override
	public List<FileRecordEntity> getFilteredFileRecordEntities() {
		
		return filteredFileRecordEntities;
	}
	
	@Override
	public FileRecordEntity getFileRecordEntityByUUID(UUID uuid) {
		
		return fileRecordEntities.stream().filter(fileRecordEntity -> fileRecordEntity.getUUID().equals(uuid)).findFirst().get();
	}
	
	@Override
	public String getDocumentPath() {
		
		return documentPath;
	}
	
	@Override
	public void setDocumentPath(String documentPath) {
		
		this.documentPath = documentPath;
	}
	
	@Override
	public String getSearchTerm() {
		
		return searchTerm;
	}

	@Override
	public void setSearchTerm(String searchTerm) {
		
		this.searchTerm = searchTerm;
	}
	
	@Override
	public FileRecordType getSelectionFileRecordType() {
		
		return selectionFileRecordType;
	}

	@Override
	public void setSelectionFileRecordType(FileRecordType selectionFileRecordType) {
		
		this.selectionFileRecordType = selectionFileRecordType;
	}
	
	@Override
	public Optional<ADetailRecord> getDetailRecordForEditing() {
		
		return detailRecordForEditing;
	}

	@Override
	public void setDetailRecordForEditing(Optional<ADetailRecord> detailRecordForEditing) {
		
		this.detailRecordForEditing = detailRecordForEditing;
	}
	
	@Override
	public void addObserver(IObserver observer) {
		
		observers.add(observer);
	}

	@Override
	public void removeObserver(IObserver observer) {
		
		observers.remove(observer);
	}

	@Override
	public void triggerEvent(EventType event) {
		
		for (IObserver observer : observers) {
			observer.handle(event);
		}
	}
}
