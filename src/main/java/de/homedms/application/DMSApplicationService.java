package de.homedms.application;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import de.homedms.HomeDMSApplication;
import de.homedms.application.subservices.DataPersistenceService;
import de.homedms.application.subservices.FileRecordDeterminationService;
import de.homedms.application.subservices.FileRecordSelectionService;
import de.homedms.application.subservices.PropertyService;
import de.homedms.application.util.EventType;
import de.homedms.application.util.IDatabase;
import de.homedms.application.util.IObservable;
import de.homedms.application.util.Language;
import de.homedms.domain.ADetailRecord;
import de.homedms.domain.FileRecordEntity;
import de.homedms.domain.FileRecordType;
import de.homedms.domain.IFileRecordRepository;
import de.homedms.domain.detailrecords.AccountStatementDetailRecordVO;
import de.homedms.domain.detailrecords.BillDetailRecordVO;
import de.homedms.domain.detailrecords.StandardDetailRecordVO;

public class DMSApplicationService {
	
	private final IFileRecordRepository fileRecordRepository;
	private final IObservable notificationProvider;
	
	private final DataPersistenceService dataPersistenceService;
	private final FileRecordDeterminationService fileRecordDeterminationService;
	private final FileRecordSelectionService fileRecordSelectionService;
	
	public DMSApplicationService(IFileRecordRepository fileRecordRepository, IObservable notificationProvider, IDatabase database) {
		
		this.fileRecordRepository = fileRecordRepository;
		this.notificationProvider = notificationProvider;
		
		this.dataPersistenceService = new DataPersistenceService(database);
		this.fileRecordDeterminationService = new FileRecordDeterminationService(this.fileRecordRepository, this.notificationProvider);
		this.fileRecordSelectionService = new FileRecordSelectionService(this.fileRecordRepository, this.notificationProvider);
		
		PropertyService.init(HomeDMSApplication.getApplicationLocationPath() + "Application.config", getClass().getResourceAsStream("/de/homedms/language/german_language_pack.config"));
		
		dataPersistenceService.initTables();
		this.submitDocumentPath(PropertyService.getProperties().getProperty(PropertyService.PROPERTY_LAST_PATH));
	}
	
	public DataPersistenceService getDataPersistenceService() {
		
		return dataPersistenceService;
	}
	
	public FileRecordSelectionService getFileRecordSelectionService() {
		
		return fileRecordSelectionService;
	}
	
	public Optional<ADetailRecord> getDetailRecordForEditing() {
		
		return fileRecordRepository.getDetailRecordForEditing();
	}
	
	public void setDetailRecordForEditing(ADetailRecord newDetailRecord) {
		
		fileRecordRepository.setDetailRecordForEditing(Optional.of(newDetailRecord));
	}
	
	public String getDocumentPath() {
		
		return fileRecordRepository.getDocumentPath();
	}
	
	public List<FileRecordEntity> getFilteredFileRecordEntities() {
		
		return fileRecordRepository.getFilteredFileRecordEntities();
	}
	
	public FileRecordEntity getFileRecordEntityByUUID(UUID uuid) {
		
		return fileRecordRepository.getFileRecordEntityByUUID(uuid);
	}
	
	public void changeTypeOfDetailRecordForEditing(FileRecordType newFileRecordType) {
		
		if (fileRecordRepository.getDetailRecordForEditing().isPresent()) {
			ADetailRecord tempDetailRecord = fileRecordRepository.getDetailRecordForEditing().get();
			
			if (tempDetailRecord.getFileRecordType() == FileRecordType.STANDARD && newFileRecordType == FileRecordType.BILL) {
				StandardDetailRecordVO standardRecord = (StandardDetailRecordVO) tempDetailRecord;
				setDetailRecordForEditing(new BillDetailRecordVO(standardRecord.getDirectoryPath(), standardRecord.getName(), standardRecord.getNotes(), Optional.empty(), Optional.empty()));
			} else if (tempDetailRecord.getFileRecordType() == FileRecordType.STANDARD && newFileRecordType == FileRecordType.ACCOUNT_STATEMENT) {
				StandardDetailRecordVO standardRecord = (StandardDetailRecordVO) tempDetailRecord;
				setDetailRecordForEditing(new AccountStatementDetailRecordVO(standardRecord.getDirectoryPath(), standardRecord.getName(), standardRecord.getNotes(), Optional.empty(), Optional.empty()));
			} else if (tempDetailRecord.getFileRecordType() == FileRecordType.BILL && newFileRecordType == FileRecordType.STANDARD) {
				BillDetailRecordVO billRecord = (BillDetailRecordVO) tempDetailRecord;
				setDetailRecordForEditing(new StandardDetailRecordVO(billRecord.getDirectoryPath(), billRecord.getName(), billRecord.getNotes()));
			} else if (tempDetailRecord.getFileRecordType() == FileRecordType.BILL && newFileRecordType == FileRecordType.ACCOUNT_STATEMENT) {
				BillDetailRecordVO billRecord = (BillDetailRecordVO) tempDetailRecord;
				setDetailRecordForEditing(new AccountStatementDetailRecordVO(billRecord.getDirectoryPath(), billRecord.getName(), billRecord.getNotes(), Optional.empty(), Optional.empty()));
			} else if (tempDetailRecord.getFileRecordType() == FileRecordType.ACCOUNT_STATEMENT && newFileRecordType == FileRecordType.STANDARD) {
				AccountStatementDetailRecordVO accRecord = (AccountStatementDetailRecordVO) tempDetailRecord;
				setDetailRecordForEditing(new StandardDetailRecordVO(accRecord.getDirectoryPath(), accRecord.getName(), accRecord.getNotes()));
			} else if (tempDetailRecord.getFileRecordType() == FileRecordType.ACCOUNT_STATEMENT && newFileRecordType == FileRecordType.BILL) {
				AccountStatementDetailRecordVO accRecord = (AccountStatementDetailRecordVO) tempDetailRecord;
				setDetailRecordForEditing(new BillDetailRecordVO(accRecord.getDirectoryPath(), accRecord.getName(), accRecord.getNotes(), Optional.empty(), Optional.empty()));
			}
			notificationProvider.triggerEvent(EventType.FILE_RECORD_SELECTED);
		}
	}
	
	public void enterDirectoryOrOpenFile(UUID uuid) {
		
		FileRecordEntity requestedEntity = fileRecordRepository.getFileRecordEntityByUUID(uuid);
		
		if (requestedEntity.getFile().isDirectory()) {
			submitDocumentPath(requestedEntity.getFile().getAbsolutePath());
		} else {
			openSystemFile(requestedEntity.getFile());
		}
	}
	
	public void submitDocumentPath(String documentPath) {
		
		if (documentPath == null) {
			documentPath = "";
		}
		
		if (isDocumentPathValid(documentPath)) {
			if (!documentPath.endsWith("\\") && documentPath.length() > 0) {
				documentPath += "\\";
			}
			fileRecordRepository.setDocumentPath(documentPath);
			fileRecordSelectionService.unselectCurrentlySelectedFileRecordEntity();
			fileRecordDeterminationService.startDeterminationProcess();
			//ermittle DetailRecords
			fileRecordRepository.getFileRecordEntities().forEach(fileRecordEntity -> {
				Optional<ADetailRecord> detailData = dataPersistenceService.getDetailRecordDataFor(fileRecordEntity.getFileRecordMetaVO().getDirectoryPath(), fileRecordEntity.getFileRecordMetaVO().getName());
				if (detailData.isPresent()) {
					fileRecordEntity.setDetailRecord(detailData.get());
				}
			});
			notificationProvider.triggerEvent(EventType.DOCUMENT_PATH_UPDATE);
			
			PropertyService.getProperties().setProperty(PropertyService.PROPERTY_LAST_PATH, documentPath);
			PropertyService.saveProperties();
		} else {
			throw new RuntimeException(Language.get(Language.ERRORMESSAGE_INVALIDDOCUMENTPATH));
		}
	}
	
	public void submitSearchTerm(String searchTerm) {
		
		fileRecordRepository.setSearchTerm(searchTerm);
		fileRecordDeterminationService.startFilteredDeterminationProcess();
		unselectEntityIfNotInFilteredEntitesAnymore();
	}
	
	public void submitSelectionFileRecordType(FileRecordType selectionFileRecordType) {
		
		fileRecordRepository.setSelectionFileRecordType(selectionFileRecordType);
		fileRecordDeterminationService.startFilteredDeterminationProcess();
		unselectEntityIfNotInFilteredEntitesAnymore();
	}
	
	private void unselectEntityIfNotInFilteredEntitesAnymore() {
		
		if (fileRecordSelectionService.getCurrentlySelectedFileRecordEntity().isPresent()) {
			if (!fileRecordRepository.getFilteredFileRecordEntities().contains(fileRecordSelectionService.getCurrentlySelectedFileRecordEntity().get())) {
				fileRecordSelectionService.unselectCurrentlySelectedFileRecordEntity();
			}
		}
	}
	
	public void openCurrentlySelectedFile() {
		
		Optional<FileRecordEntity> fileRecordEntity = fileRecordSelectionService.getCurrentlySelectedFileRecordEntity();
		if (fileRecordEntity.isPresent()) {
			openSystemFile(fileRecordEntity.get().getFile());
		}
	}
	
	public void saveDetailRecordForEditing() {
		
		if (fileRecordRepository.getDetailRecordForEditing().isPresent()) {
			dataPersistenceService.saveDetailRecordDataFor(fileRecordRepository.getDetailRecordForEditing().get());
			fileRecordSelectionService.getCurrentlySelectedFileRecordEntity().get().setDetailRecord(fileRecordRepository.getDetailRecordForEditing().get());
		}
	}
	
	public void resetDetailRecordForEditing() {
		
		Optional<ADetailRecord> oldDetailRecord = fileRecordSelectionService.getCurrentlySelectedFileRecordEntity().get().getDetailRecord();
		if (oldDetailRecord.isPresent()) {
			fileRecordRepository.setDetailRecordForEditing(Optional.of(oldDetailRecord.get()));
		} else {
			FileRecordEntity selectedFileRecordEntity = fileRecordSelectionService.getCurrentlySelectedFileRecordEntity().get();
			fileRecordRepository.setDetailRecordForEditing(Optional.of(StandardDetailRecordVO.createDefault(selectedFileRecordEntity.getFileRecordMetaVO().getDirectoryPath(), selectedFileRecordEntity.getFileRecordMetaVO().getName())));
		}
		notificationProvider.triggerEvent(EventType.FILE_RECORD_SELECTED);
	}
	
	public void deleteDetailRecord() {
		
		Optional<ADetailRecord> currentDetailRecord = fileRecordSelectionService.getCurrentlySelectedFileRecordEntity().get().getDetailRecord();
		if (currentDetailRecord.isPresent()) {
			dataPersistenceService.deleteDetailRecordDataFor(currentDetailRecord.get().getDirectoryPath(), currentDetailRecord.get().getName());
			fileRecordSelectionService.getCurrentlySelectedFileRecordEntity().get().setDetailRecord(null);
			FileRecordEntity selectedFileRecordEntity = fileRecordSelectionService.getCurrentlySelectedFileRecordEntity().get();
			fileRecordRepository.setDetailRecordForEditing(Optional.of(StandardDetailRecordVO.createDefault(selectedFileRecordEntity.getFileRecordMetaVO().getDirectoryPath(), selectedFileRecordEntity.getFileRecordMetaVO().getName())));
			notificationProvider.triggerEvent(EventType.FILE_RECORD_SELECTED);
		}
	}
	
	private boolean isDocumentPathValid(String path) {
		
		File newPath = new File(path);
		if ((newPath.exists() && !newPath.isFile()) || newPath.getPath().equals("")) {
			return true;
		} else {
			return false;
		}
	}
	
	private void openSystemFile(File file) {
		
		Desktop desktop = Desktop.getDesktop();
		
		try {
			desktop.open(file);
		} catch (IOException | IllegalArgumentException exception) {
			throw new RuntimeException(Language.get(Language.ERRORMESSAGE_FILENOTLONGERAVAILABLE));
		}
	}
}
