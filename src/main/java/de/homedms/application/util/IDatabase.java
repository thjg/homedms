package de.homedms.application.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IDatabase {
	
	public ResultSet executeSelect(String sql, String... params) throws SQLException;
	public void executeInsertOrUpdateOrDelete(String sql, String... params) throws SQLException;
	public void executeCreateTable(String sql) throws SQLException;
}
