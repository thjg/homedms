package de.homedms.application.util;

import de.homedms.application.subservices.PropertyService;
import de.homedms.domain.FileRecordType;

public enum Language {
	
	TITLE,
	ERRORMESSAGE_NOUPPERFOLDEREXISTS,
	ERRORMESSAGE_DOCUMENTPATHCACHINGFAILED,
	ERRORMESSAGE_INVALIDDOCUMENTPATH,
	ERRORMESSAGE_FILENOTLONGERAVAILABLE,
	ERRORMESSAGE_DATABASECONNECTFAILED,
	COMPONENT_DOCUMENTAREA_SEARCHFIELD_PROMPTTEXT,
	COMPONENT_DOCUMENTAREA_DOCUMENTPATHFIELD_PROMPTTEXT,
	COMPONENT_DETAILAREA_OPENFILEBUTTONDESCRIPTION,
	COMPONENT_DETAILAREA_DELETEFILEBUTTONDESCRIPTION,
	COMPONENT_DETAILAREA_SAVECHANGESBUTTONDESCRIPTION,
	COMPONENT_DETAILAREA_UNDOCHANGESBUTTONDESCRIPTION,
	COMPONENT_DETAILAREA_DELETEDATABUTTONDESCRIPTION,
	COMPONENT_DETAILAREA_LABEL_CHANGEDATE,
	COMPONENT_DETAILAREA_LABEL_FILESIZE,
	COMPONENT_DETAILAREA_LABEL_FILETYPE,
	COMPONENT_DETAILAREA_LABEL_NOTES,
	COMPONENT_DETAILAREA_LABEL_BILL_BILLDATE,
	COMPONENT_DETAILAREA_LABEL_BILL_PAYDATE,
	COMPONENT_DETAILAREA_LABEL_ACCOUNTSTATEMENT_CREATEDATE,
	COMPONENT_DETAILAREA_LABEL_ACCOUNTSTATEMENT_BANK,
	COMPONENT_FILETYPE_STANDARD,
	COMPONENT_FILETYPE_BILL,
	COMPONENT_FILETYPE_ACCOUNTSTATEMENT;
	
	Language() {
		
	}
	
	public static String get(Language textDescription) {
		
		return PropertyService.getLanguageProperties().getProperty(textDescription.name().toLowerCase());
	}
	
	public static String get(FileRecordType fileRecordType) {
		
		Language textDescription = COMPONENT_FILETYPE_STANDARD;
		
		if (fileRecordType == FileRecordType.BILL) {
			textDescription = COMPONENT_FILETYPE_BILL;
		} else if (fileRecordType == FileRecordType.ACCOUNT_STATEMENT) {
			textDescription = COMPONENT_FILETYPE_ACCOUNTSTATEMENT;
		}
		
		return get(textDescription);
	}
}
