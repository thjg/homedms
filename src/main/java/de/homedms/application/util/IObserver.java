package de.homedms.application.util;

public interface IObserver {
	
	public void handle(EventType event);
}
