package de.homedms.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Optional;

import org.junit.Test;

import de.homedms.domain.detailrecords.StandardDetailRecordVO;

public class FileRecordEntityTest {
	
	@Test
	public void testFileRecordEntityCreation() {
		
		//arrange
		File file = new File("C:/test.txt");
		
		ADetailRecord detailRecord = new StandardDetailRecordVO(file.getAbsolutePath(), file.getName(), Optional.of("Notes"));
		
		//act
		FileRecordEntity entity = new FileRecordEntity(file, detailRecord);
		
		//assert
		assertTrue(entity.getFileRecordMetaVO().getName().equals(file.getName()));
		assertTrue(entity.getDetailRecord().get().equals(detailRecord));
		assertFalse(entity.isSelected());
	}
	
}
