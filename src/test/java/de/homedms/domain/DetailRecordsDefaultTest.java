package de.homedms.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.Test;

import de.homedms.domain.detailrecords.AccountStatementDetailRecordVO;
import de.homedms.domain.detailrecords.BillDetailRecordVO;
import de.homedms.domain.detailrecords.StandardDetailRecordVO;

public class DetailRecordsDefaultTest {
	
	@Test
	public void testStandardDetailRecordForDefault() {
		
		//arrange
		StandardDetailRecordVO emptyStandardDetailRecord = new StandardDetailRecordVO("", "", Optional.empty());
		
		StandardDetailRecordVO filledStandardDetailRecord = new StandardDetailRecordVO("", "", Optional.of("Notes"));
		
		//act
		
		//assert
		assertTrue(emptyStandardDetailRecord.isDefault());
		
		assertFalse(filledStandardDetailRecord.isDefault());
	}
	
	@Test
	public void testBillDetailRecordForDefault() {
		
		//arrange
		BillDetailRecordVO emptyBillDetailRecord = new BillDetailRecordVO("", "", Optional.empty(), Optional.empty(), Optional.empty());
		
		BillDetailRecordVO partlyFilledBillDetailRecord1 = new BillDetailRecordVO("", "", Optional.of("Notes"), Optional.empty(), Optional.empty());
		BillDetailRecordVO partlyFilledBillDetailRecord2 = new BillDetailRecordVO("", "", Optional.empty(), Optional.of(LocalDate.now()), Optional.empty());
		BillDetailRecordVO partlyFilledBillDetailRecord3 = new BillDetailRecordVO("", "", Optional.empty(), Optional.empty(), Optional.of(LocalDate.now()));
		BillDetailRecordVO filledBillDetailRecord = new BillDetailRecordVO("", "", Optional.of("Notes"), Optional.of(LocalDate.now()), Optional.of(LocalDate.now()));
		
		//act
		
		//assert
		assertTrue(emptyBillDetailRecord.isDefault());
		
		assertFalse(partlyFilledBillDetailRecord1.isDefault());
		assertFalse(partlyFilledBillDetailRecord2.isDefault());
		assertFalse(partlyFilledBillDetailRecord3.isDefault());
		assertFalse(filledBillDetailRecord.isDefault());
	}
	
	@Test
	public void testAccountStatementDetailRecordForDefault() {
		
		//arrange
		AccountStatementDetailRecordVO emptyAccountStatementDetailRecord = new AccountStatementDetailRecordVO("", "", Optional.empty(), Optional.empty(), Optional.empty());
		
		AccountStatementDetailRecordVO partlyFilledAccountStatementDetailRecord1 = new AccountStatementDetailRecordVO("", "", Optional.of("Notes"), Optional.empty(), Optional.empty());
		AccountStatementDetailRecordVO partlyFilledAccountStatementDetailRecord2 = new AccountStatementDetailRecordVO("", "", Optional.empty(), Optional.of(LocalDate.now()), Optional.empty());
		AccountStatementDetailRecordVO partlyFilledAccountStatementDetailRecord3 = new AccountStatementDetailRecordVO("", "", Optional.empty(), Optional.empty(), Optional.of("Bank"));
		AccountStatementDetailRecordVO filledAccountStatementDetailRecord = new AccountStatementDetailRecordVO("", "", Optional.of("Notes"), Optional.of(LocalDate.now()), Optional.of("Bank"));
		
		//act
		
		//assert
		assertTrue(emptyAccountStatementDetailRecord.isDefault());
		
		assertFalse(partlyFilledAccountStatementDetailRecord1.isDefault());
		assertFalse(partlyFilledAccountStatementDetailRecord2.isDefault());
		assertFalse(partlyFilledAccountStatementDetailRecord3.isDefault());
		assertFalse(filledAccountStatementDetailRecord.isDefault());
	}
	
}
