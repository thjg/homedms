package de.homedms.application;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import de.homedms.application.subservices.FileRecordDeterminationService;
import de.homedms.domain.FileRecordEntity;

public class FileRecordDeterminationServiceTest {
	
	@Test
	public void testStartFilteredFileDeterminationProcessMethod() {
		
		//arrange
		FileRecordRepository repository = new FileRecordRepository();
		repository.setDocumentPath("C:\\");
		FileRecordDeterminationService determinationService = new FileRecordDeterminationService(repository, repository);
		
		//act
		repository.setSearchTerm("tes");
		repository.getFileRecordEntities().add(new FileRecordEntity(new File("C:\\test.txt"), null));
		repository.getFileRecordEntities().add(new FileRecordEntity(new File("C:\\abc.txt"), null));
		
		determinationService.startFilteredDeterminationProcess();
		
		//assert
		assertTrue(repository.getFilteredFileRecordEntities().size() == 1);
		assertTrue(repository.getFilteredFileRecordEntities().get(0).getFileRecordMetaVO().getName().equals("test.txt"));
	}
	
}
