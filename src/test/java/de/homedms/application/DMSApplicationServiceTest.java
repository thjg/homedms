package de.homedms.application;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import de.homedms.application.subservices.DataPersistenceService;
import de.homedms.application.subservices.PropertyService;
import de.homedms.domain.FileRecordEntity;
import de.homedms.domain.FileRecordType;
import de.homedms.domain.detailrecords.BillDetailRecordVO;
import de.homedms.domain.detailrecords.StandardDetailRecordVO;
import de.homedms.plugins.SQLiteDatabase;

public class DMSApplicationServiceTest {
	
	private static MockedConstruction<DataPersistenceService> mockedDataPersistenceService;
	private static MockedStatic<PropertyService> mockedPropertyService;

	@Test
	public void testChangeTypeOfDetailRecordForEditingMethod() {
		
		//arrange
		SQLiteDatabase database = Mockito.mock(SQLiteDatabase.class);
		FileRecordRepository repository = new FileRecordRepository();
		DMSApplicationService applicationService = new DMSApplicationService(repository, repository, database);
		
		//act
		applicationService.setDetailRecordForEditing(new StandardDetailRecordVO("C:\\", "test.txt", Optional.of("Notes")));
		applicationService.changeTypeOfDetailRecordForEditing(FileRecordType.BILL);
		
		//assert
		assertTrue(repository.getDetailRecordForEditing().get().getFileRecordType().equals(FileRecordType.BILL));
	}
	
	@Test
	public void testSubmitSearchTermMethod() {
		
		//arrange
		SQLiteDatabase database = Mockito.mock(SQLiteDatabase.class);
		FileRecordRepository repository = new FileRecordRepository();
		repository.setDocumentPath("C:\\");
		DMSApplicationService applicationService = new DMSApplicationService(repository, repository, database);
		
		FileRecordEntity entity1 = new FileRecordEntity(new File("C:\\test.txt"), null);
		FileRecordEntity entity2 = new FileRecordEntity(new File("C:\\abc.txt"), null);
		repository.getFileRecordEntities().add(entity1);
		repository.getFileRecordEntities().add(entity2);
		
		
		//act
		entity1.setSelectedState(true);
		applicationService.submitSearchTerm("ab");
		
		//assert
		assertFalse(entity1.isSelected());
		assertFalse(repository.getFilteredFileRecordEntities().contains(entity1));
		assertTrue(repository.getFilteredFileRecordEntities().contains(entity2));
	}
	
	@Test
	public void testSubmitSelectionFileRecordTypeMethod() {
		
		//arrange
		SQLiteDatabase database = Mockito.mock(SQLiteDatabase.class);
		FileRecordRepository repository = new FileRecordRepository();
		repository.setDocumentPath("C:\\");
		DMSApplicationService applicationService = new DMSApplicationService(repository, repository, database);
		
		FileRecordEntity entity1 = new FileRecordEntity(new File("C:\\test.txt"), new StandardDetailRecordVO("C:\\", "test.txt", Optional.of("Notes")));
		FileRecordEntity entity2 = new FileRecordEntity(new File("C:\\abc.txt"), new BillDetailRecordVO("C:\\", "abc.txt", Optional.empty(), Optional.of(LocalDate.now()), Optional.of(LocalDate.now())));
		repository.getFileRecordEntities().add(entity1);
		repository.getFileRecordEntities().add(entity2);
		
		
		//act
		entity1.setSelectedState(true);
		applicationService.submitSelectionFileRecordType(FileRecordType.BILL);
		
		//assert
		assertFalse(entity1.isSelected());
		assertFalse(repository.getFilteredFileRecordEntities().contains(entity1));
		assertTrue(repository.getFilteredFileRecordEntities().contains(entity2));
	}
	
	@Before
	public void mockDMSApplicationServiceRelatingServices() {
		
		if (mockedDataPersistenceService == null || mockedDataPersistenceService.isClosed()) {
			mockedDataPersistenceService = Mockito.mockConstruction(DataPersistenceService.class);
		}
		if (mockedPropertyService == null || mockedPropertyService.isClosed()) {
			mockedPropertyService = Mockito.mockStatic(PropertyService.class);
		}
		
		Mockito.when(PropertyService.getProperties()).thenReturn(Mockito.mock(Properties.class));
		Mockito.when(PropertyService.getProperties().getProperty(PropertyService.PROPERTY_LAST_PATH)).thenReturn("");
	}
	
	@After
	public void cleanupDMSApplicationServiceRelatingServices() {
		
		mockedDataPersistenceService.close();
		mockedPropertyService.close();
	}
}
