package de.homedms.application;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;
import org.mockito.Mockito;

import de.homedms.application.subservices.DataPersistenceService;
import de.homedms.application.util.IDatabase;
import de.homedms.domain.ADetailRecord;
import de.homedms.domain.FileRecordType;
import de.homedms.domain.detailrecords.BillDetailRecordVO;
import de.homedms.plugins.SQLiteDatabase;

public class DataPersistenceServiceTest {
	
	@Test
	public void testReceivingDetailRecordFromDatabase() throws SQLException {
		
		//arrange
		IDatabase database = Mockito.mock(SQLiteDatabase.class);
		String directoryPath = "C:\\";
		String name = "test.txt";
		DataPersistenceService dataPersistenceService = new DataPersistenceService(database);
		
		//act
		defineDatabaseBehavior(database, directoryPath, name);
		ADetailRecord detailRecord = dataPersistenceService.getDetailRecordDataFor(directoryPath, name).get();
		
		//assert
		assertTrue(detailRecord.getFileRecordType().equals(FileRecordType.BILL));
		assertTrue(((BillDetailRecordVO) detailRecord).getBillDate().isPresent());
		assertFalse(((BillDetailRecordVO) detailRecord).getPayDate().isPresent());
	}
	
	private void defineDatabaseBehavior(IDatabase database, String directoryPath, String name) throws SQLException {
		
		ResultSet mockedRS = Mockito.mock(ResultSet.class);
		Mockito.when(mockedRS.next()).thenReturn(true);
		Mockito.when(mockedRS.getString(DataPersistenceService.COL_BASE_TYPE)).thenReturn(FileRecordType.BILL.toString());
		Mockito.when(mockedRS.getString(DataPersistenceService.COL_STANDARD_NOTES)).thenReturn("Note");
		Mockito.when(mockedRS.getString(DataPersistenceService.COL_BILL_BILLDATE)).thenReturn("2020-05-03");
		Mockito.when(mockedRS.getString(DataPersistenceService.COL_BILL_PAYDATE)).thenReturn("");
		
		Mockito.when(database.executeSelect(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(mockedRS);
	}
	
}
