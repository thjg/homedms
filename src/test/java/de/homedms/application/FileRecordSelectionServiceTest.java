package de.homedms.application;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import de.homedms.application.subservices.FileRecordSelectionService;
import de.homedms.domain.FileRecordEntity;

public class FileRecordSelectionServiceTest {
	
	@Test
	public void testStartFilteredFileDeterminationProcessMethod() {
		
		//arrange
		FileRecordRepository repository = new FileRecordRepository();
		FileRecordSelectionService selectionService = new FileRecordSelectionService(repository, repository);
		FileRecordEntity entity1 = new FileRecordEntity(new File("C:\\test.txt"), null);
		FileRecordEntity entity2 = new FileRecordEntity(new File("C:\\abc.txt"), null);
		
		//act
		entity2.setSelectedState(true);
		repository.getFileRecordEntities().add(entity1);
		repository.getFileRecordEntities().add(entity2);
		
		selectionService.changeSelectionOfFileRecordByUUID(entity1.getUUID());
		
		//assert
		assertTrue(repository.getFileRecordEntityByUUID(entity1.getUUID()).isSelected());
		assertFalse(repository.getFileRecordEntityByUUID(entity2.getUUID()).isSelected());
	}
	
}
